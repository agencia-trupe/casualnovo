<!DOCTYPE html>
<html>
<head>
    <title>[NOVO CADASTRO] {{ config('app.name') }}</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana, sans-serif;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana, sans-serif;'>{{ $nome }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana, sans-serif;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana, sans-serif;'>{{ $email }}</span><br><br>
    <span style='font-size:16px;line-height:1.5;font-family:Verdana, sans-serif;'>
        <a href="{{ route('painel.area-profissional.usuarios.show', $id) }}">Clique aqui para ver o usuário no painel.</a>
    </span>
</body>
</html>
