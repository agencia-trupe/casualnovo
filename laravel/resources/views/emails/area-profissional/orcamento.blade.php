<!DOCTYPE html>
<html>
<head>
    <title>[ORÇAMENTO] {{ config('app.name') }}</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:15px;font-family:Verdana, sans-serif;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana, sans-serif;'>{{ $orcamento->usuario->nome }}</span><br>
    <span style='font-weight:bold;font-size:15px;font-family:Verdana, sans-serif;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana, sans-serif;'>{{ $orcamento->usuario->email }}</span><br>
    <span style='font-weight:bold;font-size:15px;font-family:Verdana, sans-serif;'>Telefone:</span> <span style='color:#000;font-size:14px;font-family:Verdana, sans-serif;'>{{ $orcamento->usuario->telefone }}</span>
    <br><br>
    <hr>
    <ul>
        @foreach(json_decode($orcamento->orcamento) as $item)
        <li style="color:#000;font-size:15px;font-family:Verdana, sans-serif;">{{ $item->codigo }} - Quantidade: {{ $item->quantidade }}</li>
        @endforeach
    </ul>
    <hr>
    <br>
    @if($orcamento->observacoes)
    <span style='font-weight:bold;font-size:15px;font-family:Verdana, sans-serif;'>Observações:</span><br><span style='color:#000;font-size:14px;font-family:Verdana, sans-serif;'>{!! nl2br($orcamento->observacoes) !!}</span>
    @endif
    <br><br>
    <span style='font-size:15px;line-height:1.5;font-family:Verdana, sans-serif;'>
        <a href="{{ route('painel.area-profissional.orcamentos.show', $orcamento->id) }}">Clique aqui para ver o orçamento no painel.</a>
    </span>
</body>
</html>
