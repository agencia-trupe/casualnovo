@extends('frontend.common.template')

@section('content')

    <div class="designers-show">
        <div class="center">
            <div class="designer">
                <img src="{{ asset('assets/img/designers/'.$designer->foto) }}" alt="">
                <span>{{ $designer->nome }}</span>
            </div>

            <div class="texto">{!! tobj($designer, 'texto') !!}</div>

            <div class="imagens">
                @foreach($designer->imagens as $imagem)
                <a href="{{ asset('assets/img/designers/imagens/'.$imagem->imagem) }}" class="fancybox" rel="designer">
                    <img src="{{ asset('assets/img/designers/imagens/thumbs/'.$imagem->imagem) }}" alt="">
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
