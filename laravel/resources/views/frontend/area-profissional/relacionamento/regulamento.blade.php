@extends('frontend.common.template')

@section('content')

	<div class="area-profissional">
        <div class="center">
            <aside>
                <h2>{{ t('nav.catalogo') }}</h2>
                @include('frontend.area-profissional._nav')
            </aside>

            <main class="relacionamento">
                <div class="wrapper">
                    @include('frontend.area-profissional.relacionamento._header')

                    <a href="{{ route('area-profissional.relacionamento.extrato') }}" class="btn-relacionamento">
                        {{ t('relacionamento.consultar-extrato') }}
                    </a>

                    <div class="regulamento">
                        <h1>{{ t('relacionamento.regulamento') }}</h1>

                        @if(auth('profissional')->user()->tipo_participacao_relacionamento == 'arquiteto')
                            {!! tobj($campanha, 'regulamento_arquiteto') !!}
                        @else
                            {!! tobj($campanha, 'regulamento_assistente') !!}
                        @endif

                        <div class="premios">
                            <h1>{{ t('relacionamento.faixas-premiacao') }}</h1>

                            <div class="premios-grid">
                                @foreach($premios as $premio)
                                <div class="premio">
                                    <p>
                                        <strong>
                                            {{ t('relacionamento.faixa-premio', [
                                                'inicio' => $premio->inicio_pontuacao,
                                                'fim'    => $premio->fim_pontuacao,
                                            ]) }}
                                        </strong>
                                        {{ $premio->titulo }}
                                    </p>
                                    <img src="{{ prod_url('assets/images/programapremios/'.$premio->imagem) }}" alt="{{ $premio->titulo }}">
                                </div>
                                @endforeach
                            </div>
                        </div>

                        <a href="{{ route('area-profissional.relacionamento') }}" class="btn-relacionamento">
                            {{ t('relacionamento.fechar-regulamento') }}
                        </a>
                    </div>
                </div>
            </main>
        </div>
	</div>

@stop
