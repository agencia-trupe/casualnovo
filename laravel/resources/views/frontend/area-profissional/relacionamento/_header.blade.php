<div class="titulo">
    <h2>{{ tobj($campanha, 'titulo') }}</h2>
    <p>{{ t('relacionamento.termino', ['dias' => $diasFaltantes]) }}</p>
</div>

<div class="saldo">
    <div class="box">
        <div class="total">
            {{ t('relacionamento.seu-total') }}
            <div>
                <span>{{ auth('profissional')->user()->pontuacaoTotal() }}</span>
                {{ t('relacionamento.pontos') }}
            </div>
        </div>
        <div class="proximo">
            <div>
                @if($premio)
                <div class="premio-atual">
                    <strong>{{ t('relacionamento.premio-atual') }}</strong>
                    {{ $premio->titulo }}
                </div>
                @endif

                @if($proximoPremio)
                <p class="proxima-faixa">
                    {{ t('relacionamento.proxima-faixa', [
                        'pontos' => money_format('%!i', ($proximoPremio->getOriginal('inicio_pontuacao') - ($saldoTotal * 100)) / 100)
                    ]) }}
                </p>
                @endif
            </div>

            @if($premio)
            <img src="{{ prod_url('assets/images/programapremios/'.$premio->imagem) }}" alt="{{ $premio->titulo }}">
            @endif
        </div>
    </div>
</div>
