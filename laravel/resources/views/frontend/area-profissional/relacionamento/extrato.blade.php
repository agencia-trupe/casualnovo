@extends('frontend.common.template')

@section('content')

	<div class="area-profissional">
        <div class="center">
            <aside>
                <h2>{{ t('nav.catalogo') }}</h2>
                @include('frontend.area-profissional._nav')
            </aside>

            <main class="relacionamento">
                <div class="wrapper">
                    @include('frontend.area-profissional.relacionamento._header')

                    @if(count(auth('profissional')->user()->pontuacao))
                    <table class="extrato">
                        <thead>
                            <tr style="text-transform:uppercase">
                                <th>{{ t('relacionamento.data') }}</th>
                                <th>{{ t('relacionamento.descricao') }}</th>
                                <th>{{ t('relacionamento.pedido') }}</th>
                                <th>{{ t('relacionamento.pontos') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach(auth('profissional')->user()->pontuacao as $ponto)
                            <tr>
                                <td>{{ Date('d/m/Y', strtotime($ponto->data_insercao)) }}</td>
                                <td>{{ $ponto->descricao }}</td>
                                <td>{{ $ponto->pedido }}</td>
                                <td>{{ $ponto->pontos }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @endif

                    <a href="{{ route('area-profissional.relacionamento') }}" class="btn-relacionamento">
                        {{ t('relacionamento.fechar-extrato') }}
                    </a>

                    <div class="chamada-regulamento">
                        <p>{{ t('relacionamento.consulte-regulamento') }}</p>
                        <a href="{{ route('area-profissional.relacionamento.regulamento') }}" class="btn-relacionamento">
                            {{ t('relacionamento.ver-regulamento') }}
                        </a>
                    </div>
                </div>
            </main>
        </div>
	</div>

@endsection
