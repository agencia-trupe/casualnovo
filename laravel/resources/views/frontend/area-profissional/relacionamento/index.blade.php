@extends('frontend.common.template')

@section('content')

	<div class="area-profissional">
        <div class="center">
            <aside>
                <h2>{{ t('nav.catalogo') }}</h2>
                @include('frontend.area-profissional._nav')
            </aside>

            <main class="relacionamento">
                <div class="wrapper">
                    @include('frontend.area-profissional.relacionamento._header')

                    <a href="{{ route('area-profissional.relacionamento.extrato') }}" class="btn-relacionamento">
                        {{ t('relacionamento.consultar-extrato') }}
                    </a>

                    <div class="chamada-regulamento">
                        <p>{{ t('relacionamento.consulte-regulamento') }}</p>
                        <a href="{{ route('area-profissional.relacionamento.regulamento') }}" class="btn-relacionamento">
                            {{ t('relacionamento.ver-regulamento') }}
                        </a>
                    </div>
                </div>
            </main>
        </div>
	</div>

@stop
