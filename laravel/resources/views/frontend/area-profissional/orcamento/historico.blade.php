@extends('frontend.common.template')

@section('content')

    <div class="area-profissional">
        <div class="center">
            <aside>
                <h2>{{ t('nav.catalogo') }}</h2>
                @include('frontend.area-profissional._nav')
            </aside>

            <main class="orcamento">
                <div>
                    @if(count($orcamentos))
                    <div class="orcamento-pedido">
                        <p>{{ t('profissional.historico') }}</p>

                        <div class="orcamento-lista">
                            @foreach($orcamentos as $orcamento)
                            <div class="orcamento-item">
                                <div>
                                    {{ $orcamento->created_at->format('d/m/Y') }}
                                    &middot;
                                    {{ $orcamento->itens_count }}
                                    {{ $orcamento->itens_count > 1 ? 'itens' : 'item' }}
                                </div>
                                <a href="{{ route('area-profissional.orcamento.historico', $orcamento->id) }}" class="btn-visualizar">
                                    {{ t('profissional.visualizar') }}
                                </a>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    @else
                    <div class="orcamento-vazio">
                        {!! t('profissional.historico-vazio') !!}
                    </div>
                    @endif
                </div>
            </main>
        </div>
    </div>

@endsection
