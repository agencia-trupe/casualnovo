@extends('frontend.common.template')

@section('content')

    <div class="area-profissional">
        <div class="center">
            <aside>
                <h2>{{ t('nav.catalogo') }}</h2>
                @include('frontend.area-profissional._nav')
            </aside>

            <main class="orcamento">
                <div>
                    @if(session('enviado'))
                        <div class="flash flash-success">{{ t('profissional.orcamento-enviado') }}</div>
                    @endif
                    @if($errors->any())
                        <div class="flash flash-error">
                            @foreach($errors->all() as $error)
                            {!! $error !!}<br>
                            @endforeach
                        </div>
                    @endif

                    @if(count($orcamento))
                    <div class="orcamento-pedido">
                        <p>{{ t('profissional.pedido-orcamento') }}</p>

                        <form action="{{ route('area-profissional.orcamento.post') }}" method="POST">
                            {!! csrf_field() !!}

                            <div class="orcamento-lista">
                                @foreach($itens as $key => $item)
                                <div class="orcamento-item">
                                    <a href="{{ route('produtos.tipo', [$item->tipo->slug, $item->codigo]) }}">
                                        <img src="{{ prod_url('assets/img/produtos/capas/'.$item->imagem_capa) }}">
                                    </a>
                                    <div>
                                        {{ $item->codigo }}
                                        @foreach(explode(',', $item->divisao) as $divisao)
                                            <span>{{ t('nav.'.$divisao) }}</span>
                                        @endforeach
                                    </div>
                                    <div class="input-quantidade">
                                        <input type="number" name="quantidade[]" value="{{ old('quantidade.'.$key) ?: '1' }}" min="1" required>
                                    </div>
                                    <a href="{{ route('area-profissional.orcamento.remover', $item->id) }}" class="btn-excluir">
                                        {{ t('profissional.excluir') }}
                                    </a>
                                </div>
                                @endforeach
                            </div>

                            <textarea name="observacoes" placeholder="{{ t('profissional.observacoes') }}">{{ old('observacoes') }}</textarea>

                            <input type="submit" class="orcamento-btn" value="{{ t('profissional.enviar-pedido') }}">
                        </form>
                    </div>
                    @else
                    <div class="orcamento-vazio">
                        {!! t('profissional.orcamento-vazio') !!}
                    </div>
                    @endif
                </div>
            </main>
        </div>
    </div>

@endsection
