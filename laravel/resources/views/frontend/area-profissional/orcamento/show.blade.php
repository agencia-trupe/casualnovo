@extends('frontend.common.template')

@section('content')

    <div class="area-profissional">
        <div class="center">
            <aside>
                <h2>{{ t('nav.catalogo') }}</h2>
                @include('frontend.area-profissional._nav')
            </aside>

            <main class="orcamento">
                <div class="orcamento-pedido">
                    <p>
                        {{ t('profissional.enviado-em') }}
                        <strong>{{ $orcamentoHistorico->created_at->format('d/m/Y') }}</strong>
                    </p>

                    <div class="orcamento-lista">
                        @foreach($orcamentoHistorico->itens as $item)
                        <div class="orcamento-item">
                            @if($item->produto)
                            <a href="{{ route('produtos.tipo', [$item->produto->tipo->slug, $item->produto->codigo]) }}">
                                <img src="{{ prod_url('assets/img/produtos/capas/'.$item->produto->imagem_capa) }}">
                            </a>
                            @else
                                <img src="{{ prod_url('assets/img/produtos/capas/'.$item->imagem) }}">
                            @endif
                            <div>
                                {{ $item->codigo }}
                                @foreach(explode(',', $item->divisao) as $divisao)
                                <span>{{ t('nav.'.$divisao) }}</span>
                                @endforeach
                            </div>
                            <div class="quantidade">{{ $item->quantidade }}</div>
                        </div>
                        @endforeach
                    </div>

                    @if($orcamentoHistorico->observacoes)
                        <div class="observacoes">
                            <small>{{ t('profissional.observacoes') }}</small>
                            {!! nl2br($orcamentoHistorico->observacoes) !!}
                        </div>
                    @endif

                    <a href="{{ route('area-profissional.orcamento.historico') }}" class="orcamento-btn">
                        {{ t('profissional.voltar') }}
                    </a>
                </div>
            </main>
        </div>
    </div>

@endsection
