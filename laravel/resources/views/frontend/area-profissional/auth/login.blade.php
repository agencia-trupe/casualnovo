@extends('frontend.common.template')

@section('content')

    <div class="area-profissional">
        <div class="center">
            <aside>
                <h2>{{ t('nav.catalogo') }}</h2>
            </aside>

            <main class="login">
                <div class="login-register">
                    <p>{{ t('profissional.texto-login') }}</p>
                    <a href="{{ route('area-profissional.register') }}" class="btn-cadastro">
                        {{ t('profissional.novo-cadastro') }}
                    </a>
                </div>

                <div class="login-box">
                    <p>{{ t('profissional.acesse') }}</p>

                    {!! Form::open(['route' => 'area-profissional.auth']) !!}
                        @if(session('error'))
                        <div class="flash flash-error">{{ session('error') }}</div>
                        @endif
                        @if(session('reset'))
                        <div class="flash flash-success">{{ session('reset') }}</div>
                        @endif

                        <input type="email" name="email" placeholder="login (e-mail)" value="{{ old('email') }}" required>
                        <input type="password" name="password" placeholder="{{ t('profissional.senha') }}" required>
                        <input type="submit" value="{{ t('profissional.entrar') }}">

                        <a href="{{ route('area-profissional.reset') }}" class="btn-esqueci">
                            {{ t('profissional.esqueci') }} &raquo;
                        </a>
                    {!! Form::close() !!}
                </div>
            </main>
        </div>
    </div>

    <div class="area-profissional-imagem" style="background-image:url({{ asset('assets/img/imagem-de-capa/'.$capa->imagem) }})"></div>

@endsection
