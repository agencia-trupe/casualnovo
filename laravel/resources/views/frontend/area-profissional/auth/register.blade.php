@extends('frontend.common.template')

@section('content')

    <div class="area-profissional">
        <div class="center">
            <aside>
                <h2>{{ t('nav.catalogo') }}</h2>
            </aside>

            <main class="register">
                <div class="wrapper">
                    {!! Form::open(['route' => 'area-profissional.register.post']) !!}

                    @if($errors->any())
                    <div class="flash flash-error">
                        @foreach($errors->all() as $error)
                        {!! $error !!}<br>
                        @endforeach
                    </div>
                    @endif

                    <div class="register-box">
                        <p>{{ t('profissional.dados-pessoais') }}</p>

                        <div class="row">
                            <label>{{ t('profissional.nome') }}</label>
                            <input type="text" name="nome" value="{{ old('nome') }}">
                        </div>
                        <div class="row">
                            <label>e-mail</label>
                            <input type="email" name="email" value="{{ old('email') }}">
                        </div>
                        <div class="row">
                            <label>cpf</label>
                            <input type="text" name="cpf" value="{{ old('cpf') }}">
                        </div>
                        <div class="row">
                            <label>{{ t('profissional.telefone') }}</label>
                            <input type="text" name="telefone" value="{{ old('telefone') }}">
                        </div>
                        <div class="row">
                            <label>{{ t('profissional.empresa') }}</label>
                            <input type="text" name="empresa" value="{{ old('empresa') }}">
                        </div>
                        <div class="row">
                            <label>{{ t('profissional.cargo') }}</label>
                            <input type="text" name="cargo" value="{{ old('cargo') }}">
                        </div>
                        <div class="row">
                            <label>{{ t('profissional.pais') }}</label>
                            <select name="pais">
                                <option value=""></option>
                                @foreach(Tools::listaPaises() as $key => $pais)
                                <option value="{{ $key }}" @if(old('pais') == $key) selected @endif>
                                    {{ $pais }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="row">
                            <label>{{ t('profissional.cep') }}</label>
                            <input type="text" name="cep" value="{{ old('cep') }}">
                        </div>
                        <div class="row">
                            <label>{{ t('profissional.endereco') }}</label>
                            <input type="text" name="endereco" value="{{ old('endereco') }}">
                        </div>
                        <div class="row">
                            <label>{{ t('profissional.numero') }}</label>
                            <input type="text" name="numero" value="{{ old('numero') }}">
                        </div>
                        <div class="row">
                            <label>{{ t('profissional.complemento') }}</label>
                            <input type="text" name="complemento" value="{{ old('complemento') }}">
                        </div>
                        <div class="row">
                            <label>{{ t('profissional.cidade') }}</label>
                            <input type="text" name="cidade" value="{{ old('cidade') }}">
                        </div>
                        <div class="row">
                            <label>{{ t('profissional.estado') }}</label>
                            <input type="text" name="estado" value="{{ old('estado') }}" maxlength="2">
                        </div>
                    </div>

                    <div class="register-box">
                        <p>{{ t('profissional.senha') }}</p>

                        <div class="row">
                            <label>{{ t('profissional.senha') }}</label>
                            <input type="password" name="password">
                        </div>
                        <div class="row">
                            <label>{{ t('profissional.confirmacao') }}</label>
                            <input type="password" name="password_confirmation">
                        </div>
                    </div>

                    <div class="register-box">
                        <p>{{ t('profissional.newsletter') }}</p>

                        <label class="checkbox">
                            <input type="hidden" name="receber_newsletter" value="0">
                            <input type="checkbox" name="receber_newsletter" value="1" @if(old('receber_newsletter') != '0') checked @endif>
                            {{ t('profissional.newsletter-receber') }}
                        </label>
                    </div>

                    <input type="submit" value="{{ t('profissional.cadastrar') }}">

                    {!! Form::close() !!}
                </div>
            </main>
        </div>
    </div>

@endsection
