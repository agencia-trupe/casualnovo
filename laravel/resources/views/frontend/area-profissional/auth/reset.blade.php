@extends('frontend.common.template')

@section('content')

    <div class="area-profissional">
        <div class="center">
            <aside>
                <h2>{{ t('nav.catalogo') }}</h2>
            </aside>

            <main class="forgot-password">
                <div class="login-box">
                    <p>{{ t('profissional.redefinir-senha') }}</p>

                    {!! Form::open(['route' => 'area-profissional.reset.post']) !!}
                        @if($errors->any())
                        <div class="flash flash-error">{!! $errors->first() !!}</div>
                        @endif

                        <input type="hidden" name="token" value="{{ $token }}">
                        <input type="hidden" name="email" value="{{ $email }}" required>
                        <input type="password" name="password" placeholder="senha" required>
                        <input type="password" name="password_confirmation" placeholder="{{ t('profissional.confirmar-senha') }}" required>
                        <input type="submit" value="{{ t('profissional.redefinir-senha') }}">
                    {!! Form::close() !!}
                </div>
            </main>
        </div>
    </div>

    <div class="area-profissional-imagem" style="background-image:url({{ asset('assets/img/imagem-de-capa/'.$capa->imagem) }})"></div>

@endsection
