@extends('frontend.common.template')

@section('content')

    <div class="area-profissional">
        <div class="center">
            <aside>
                <h2>{{ t('nav.catalogo') }}</h2>
            </aside>

            <main class="forgot-password">
                <div class="login-box">
                    <p>{{ t('profissional.esqueci') }}</p>

                    {!! Form::open(['route' => 'area-profissional.reset.email']) !!}
                        @if(session('sent'))
                            <div class="flash flash-success" style="margin-bottom:0">{{ session('sent') }}</div>
                        @else
                            @if($errors->any())
                            <div class="flash flash-error">{{ $errors->first() }}</div>
                            @endif
                            @if(session('error'))
                            <div class="flash flash-error">{{ session('error') }}</div>
                            @endif

                            <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                            <input type="submit" value="{{ t('profissional.redefinir-senha') }}">
                        @endif
                    {!! Form::close() !!}
                </div>
            </main>
        </div>
    </div>

    <div class="area-profissional-imagem" style="background-image:url({{ asset('assets/img/imagem-de-capa/'.$capa->imagem) }})"></div>

@endsection
