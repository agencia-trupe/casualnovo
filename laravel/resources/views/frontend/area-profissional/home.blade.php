@extends('frontend.common.template')

@section('content')

    <div class="area-profissional">
        <div class="center">
            <aside>
                <h2>{{ t('nav.catalogo') }}</h2>
                @include('frontend.area-profissional._nav')
            </aside>

            <main class="profissional-home">
                <div>
                    <div class="texto">{!! tobj($home, 'texto') !!}</div>

                    @if(tobj($home, 'imagem'))
                        <img src="{{ asset('assets/img/tela-inicial/'.tobj($home, 'imagem')) }}" alt="">
                    @endif
                </div>
            </main>
        </div>
    </div>

@endsection
