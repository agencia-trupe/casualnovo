<nav>
    <a href="{{ route('area-profissional.meu-cadastro') }}" @if(Tools::routeIs('area-profissional.meu-cadastro')) class="active" @endif>
        {{ t('profissional.meu-cadastro') }}
    </a>

    @if(auth('profissional')->user()->podeSolicitarOrcamento)
        <a href="{{ route('area-profissional.orcamento') }}" @if(Tools::routeIs('area-profissional.orcamento')) class="active" @endif>
            {{ t('profissional.solicitar-orcamento') }}
        </a>

        <a href="{{ route('area-profissional.orcamento.historico') }}" @if(Tools::routeIs('area-profissional.orcamento.historico')) class="active" @endif>
            {{ t('profissional.historico') }}
        </a>
    @endif

    @if(auth('profissional')->user()->participaProgramaRelacionamento)
        <a href="{{ route('area-profissional.relacionamento') }}" @if(Tools::routeIs('area-profissional.relacionamento*')) class="active" @endif>
            {{ t('profissional.relacionamento') }}
        </a>
    @endif
</nav>
