@extends('frontend.common.template')

@section('content')

    <div class="area-profissional">
        <div class="center">
            <aside>
                <h2>{{ t('nav.catalogo') }}</h2>
                @include('frontend.area-profissional._nav')
            </aside>

            <main class="register">
                <div class="wrapper">
                    {!! Form::open(['route' => 'area-profissional.meu-cadastro.update', 'method' => 'patch']) !!}

                    @if($errors->any())
                    <div class="flash flash-error">
                        @foreach($errors->all() as $error)
                        {!! $error !!}<br>
                        @endforeach
                    </div>
                    @endif
                    @if(session('success'))
                        <div class="flash flash-success">
                            {{ session('success') }}
                        </div>
                    @endif

                    <div class="register-box">
                        <p>{{ t('profissional.dados-pessoais') }}</p>

                        <div class="row">
                            <label>{{ t('profissional.nome') }}</label>
                            <input type="text" name="nome" value="{{ $user->nome }}">
                        </div>
                        <div class="row">
                            <label>e-mail</label>
                            <input type="email" name="email" value="{{ $user->email }}">
                        </div>
                        <div class="row">
                            <label>cpf</label>
                            <input type="text" name="cpf" value="{{ $user->cpf }}">
                        </div>
                        <div class="row">
                            <label>{{ t('profissional.telefone') }}</label>
                            <input type="text" name="telefone" value="{{ $user->telefone }}">
                        </div>
                        <div class="row">
                            <label>{{ t('profissional.empresa') }}</label>
                            <input type="text" name="empresa" value="{{ $user->empresa }}">
                        </div>
                        <div class="row">
                            <label>{{ t('profissional.cargo') }}</label>
                            <input type="text" name="cargo" value="{{ $user->cargo }}">
                        </div>
                        <div class="row">
                            <label>{{ t('profissional.pais') }}</label>
                            <select name="pais">
                                <option value=""></option>
                                @foreach(Tools::listaPaises() as $key => $pais)
                                <option value="{{ $key }}" @if($user->pais == $key) selected @endif>
                                    {{ $pais }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="row">
                            <label>{{ t('profissional.cep') }}</label>
                            <input type="text" name="cep" value="{{ $user->cep }}">
                        </div>
                        <div class="row">
                            <label>{{ t('profissional.endereco') }}</label>
                            <input type="text" name="endereco" value="{{ $user->endereco }}">
                        </div>
                        <div class="row">
                            <label>{{ t('profissional.numero') }}</label>
                            <input type="text" name="numero" value="{{ $user->numero }}">
                        </div>
                        <div class="row">
                            <label>{{ t('profissional.complemento') }}</label>
                            <input type="text" name="complemento" value="{{ $user->complemento }}">
                        </div>
                        <div class="row">
                            <label>{{ t('profissional.cidade') }}</label>
                            <input type="text" name="cidade" value="{{ $user->cidade }}">
                        </div>
                        <div class="row">
                            <label>{{ t('profissional.estado') }}</label>
                            <input type="text" name="estado" value="{{ $user->estado }}" maxlength="2">
                        </div>
                    </div>

                    <div class="register-box">
                        <p>{{ t('profissional.alteracao-senha') }}</p>

                        <div class="row">
                            <label>{{ t('profissional.nova-senha') }}</label>
                            <input type="password" name="password">
                        </div>
                        <div class="row">
                            <label>{{ t('profissional.confirmacao') }}</label>
                            <input type="password" name="password_confirmation">
                        </div>
                    </div>

                    <div class="register-box">
                        <p>{{ t('profissional.newsletter') }}</p>

                        <label class="checkbox">
                            <input type="hidden" name="receber_newsletter" value="0">
                            <input type="checkbox" name="receber_newsletter" value="1" @if($user->receber_newsletter) checked @endif>
                            {{ t('profissional.newsletter-receber') }}
                        </label>
                    </div>

                    <input type="submit" value="{{ t('profissional.salvar-alteracoes') }}">

                    {!! Form::close() !!}
                </div>
            </main>
        </div>
    </div>

@endsection
