@extends('frontend.common.template')

@section('content')

    <div class="contato center">
        @if(count($enderecos))
        <div class="enderecos">
            <nav>
                @foreach($enderecos as $endereco)
                <a href="#" data-mapa="{{ $endereco->google_maps }}" @if($endereco->id === $enderecos->first()->id) class="active" @endif>
                    <span>{{ tobj($endereco, 'titulo') }}</span>
                </a>
                @endforeach
            </nav>
            <div class="mapa">
                {!! $enderecos->first()->google_maps !!}
            </div>
        </div>
        @endif

        <form action="{{ route('contato.post') }}" method="POST">
            <p>{{ t('contato.fale-conosco') }}</p>

            @if(session('enviado'))
            <div class="flash flash-sucesso">{{ t('contato.sucesso') }}</div>
            @endif
            @if($errors->any())
            <div class="flash flash-erro">{{ t('contato.erro') }}</div>
            @endif

            {!! csrf_field() !!}
            <input type="text" name="nome" placeholder="{{ t('contato.nome') }}" required value="{{ old('nome') }}">
            <input type="email" name="email" placeholder="e-mail" required value="{{ old('email') }}">
            <input type="text" name="telefone" placeholder="{{ t('contato.telefone') }}" value="{{ old('telefone') }}">
            <textarea name="mensagem" placeholder="{{ t('contato.mensagem') }}" required>{{ old('mensagem') }}</textarea>
            <input type="submit" value="{{ t('contato.enviar') }}">
        </form>
    </div>

@endsection
