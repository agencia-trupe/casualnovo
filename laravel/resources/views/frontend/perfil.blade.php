@extends('frontend.common.template')

@section('content')

    <div class="perfil center">
        <div class="texto">
            {!! tobj($perfil, 'texto') !!}
        </div>

        <div class="imagem">
            <img src="{{ asset('assets/img/perfil/'.$perfil->imagem) }}" alt="">
        </div>
    </div>

@endsection
