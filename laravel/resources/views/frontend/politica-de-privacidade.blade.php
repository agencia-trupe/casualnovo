@extends('frontend.common.template')

@section('content')

<div class="politica-de-privacidade center">
    <h2 class="titulo">{{ t('nav.politica') }}</h2>
    <div class="texto">
        {!! tobj($politica, 'texto') !!}
    </div>
</div>

@endsection