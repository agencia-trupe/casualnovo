<div class="form-busca">
    @if(isset($tipo))
    <div class="filtros">
        @foreach(divisoes() as $divisao)
        <label class="radio">
            <input type="radio" name="divisao" value="{{ $divisao }}" class="radio-route" data-route="{{ route('produtos.tipo', ['tipo' => $tipo->slug, 'codigo' => null, 'divisao' => $divisao]) }}" @if(request('divisao') === $divisao) checked @endif>
            <div class="custom-radio"></div>
            <span>{{ t('nav.'.$divisao) }}</span>
        </label>
        @endforeach
    </div>
    @endif
    <form action="{{ route('produtos.busca') }}" method="GET">
        @if(!isset($tipo))
        <div class="filtros">
            @foreach(divisoes() as $divisao)
            <label class="radio">
                <input type="radio" name="divisao" value="{{ $divisao }}" @if(request('divisao') === $divisao) checked @endif>
                <div class="custom-radio"></div>
                <span>{{ t('nav.'.$divisao) }}</span>
            </label>
            @endforeach
        </div>
        @else
        <input type="hidden" name="divisao" value="{{ request('divisao') }}">
        @endif
        <input type="text" name="codigo" placeholder="{{ t('catalogo.busca') }}" value="{{ request('codigo') }}" required>
        <button type="submit"></button>
    </form>
</div>
