@extends('frontend.common.template')

@section('content')

<div class="catalogo catalogo-index">
    <div class="center">
        <div class="top-bar">
            <h2>
                {{ t('nav.produtos') }} &middot;
                {{ tobj($tipo, 'titulo') }}
            </h2>

            @include('frontend.catalogo._form-busca')
        </div>
    </div>

    <div class="produto center">
        <div class="left">
            @if(auth('profissional')->check() && auth('profissional')->user()->is_funcionario)
            <img src="{{ prod_url('assets/img/produtos/redimensionadas/'.$produto->imagem_capa) }}" class="no-margin" alt="">
            <div class="exibicao-funcionario">
                <img src="{{ asset('assets/img/fornecedores/'.($produto->fornecedor ? $produto->fornecedor->imagem : '')) }}" alt="">
                <div>
                    @if($produto->item)
                    <h1>{{ tobj($produto, 'item') }} • {{ $produto->codigo }}</h1>
                    @else
                    <h1>{{ $produto->codigo }}</h1>
                    @endif
                    <p>
                        @foreach(explode(',', $produto->divisao) as $divisao)
                        <span>{{ t('nav.'.$divisao) }}</span>
                        @endforeach
                    </p>

                    <p class="titulo">{{ tobj($produto, 'titulo') }}</p>
                    @if($produto->fornecedor)<p class="marca">{{ $produto->fornecedor->titulo }}</p>@endif
                    <p class="descritivo">{!! tobj($produto, 'descritivo') !!}</p>

                    <ul>
                        @if($produto->designer)
                        <li>Designer: {{ $produto->designer }}</li>
                        @endif
                        @if($produto->dimensoes)
                        <li>{{ t('catalogo.dimensoes') }}:<br>{!! $produto->dimensoes !!}</li>
                        @endif
                        @if(count($produto->materiais))
                        <li>
                            {{ t('catalogo.materiais') }}:
                            <ul>
                                @foreach($produto->materiais as $m)
                                <li>{{ tobj($m, 'titulo') }}</li>
                                @endforeach
                            </ul>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
            @else
            <img src="{{ prod_url('assets/img/produtos/redimensionadas/'.$produto->imagem_capa) }}" alt="">
            @if($produto->item)
            <h1>{{ tobj($produto, 'item') }} • {{ $produto->codigo }}</h1>
            @else
            <h1>{{ $produto->codigo }}</h1>
            @endif
            <p>
                @foreach(explode(',', $produto->divisao) as $divisao)
                <span>{{ t('nav.'.$divisao) }}</span>
                @endforeach
            </p>
            @if(auth('profissional')->check())
            <p class="descritivo">{!! tobj($produto, 'descritivo') !!}</p>
            @endif
            @endif

            @if($produto->exibir_3d)
            <div id="container-3d"></div>
            <script>
                var $3dViewer = document.getElementById('container-3d');

                R2U.init({
                    customerId: '{{ env('
                    R2U_CUSTOMER_ID ') }}'
                }).then((data) => {
                    R2U.sku.isActive('{{ $produto->codigo }}').then((isActive) => {
                        if (!isActive) {
                            return $3dViewer.remove();
                        }

                        R2U.viewer.create({
                            element: $3dViewer,
                            sku: '{{ $produto->codigo }}',
                            name: '{{ tobj($produto, '
                            titulo ') }}',
                            popup: false,
                        });
                    }).catch((err) => {
                        $3dViewer.remove();
                    });
                });
            </script>
            @endif

            @if(auth('profissional')->check() && auth('profissional')->user()->podeSolicitarOrcamento)
            <a href="#" class="btn-adicionar-orcamento @if(in_array($produto->id, $orcamento)) active @endif" data-id="{{ $produto->id }}" data-add-url="{{ route('area-profissional.orcamento.adicionar', $produto->id) }}" data-list-url="{{ route('area-profissional.orcamento') }}">
                <span>
                    {{ t('catalogo.adicionar') }}
                </span>
                <span class="active">
                    {{ t('catalogo.adicionado') }}
                </span>
            </a>
            @endif
        </div>
        <div class="right">
            @foreach($produto->imagens as $imagem)
            <img src="{{ prod_url('assets/img/produtos/redimensionadas/'.$imagem->imagem) }}" alt="">
            @endforeach
        </div>
    </div>

    @if(count($produtos) > 1)
    <div class="mais-opcoes">
        <div class="center">
            <h3>
                {{ t('catalogo.mais-opcoes') }}
                {{ tobj($tipo, 'titulo') }}
            </h3>
        </div>
    </div>

    <div class="produtos produtos-small">
        @foreach($produtos->filter(function($o) use ($produto) { return $o->id !== $produto->id; })->shuffle()->chunk(48) as $page => $chunk)
        @foreach($chunk as $p)
        <a href="{{ route('produtos.tipo', [$tipo->slug, $p->codigo]) }}" data-page="{{ $page }}" @if($page===0) class="visible" @endif>
            <div class="imagem" style="background-image:url('{{ prod_url('assets/img/produtos/capas/'.$p->imagem_capa) }}')"></div>
            <div class="overlay">
                <span>{{ $p->codigo }}</span>
            </div>
            {{ tobj($p, 'titulo') }}
        </a>
        @endforeach
        @endforeach
    </div>
    @endif

    <a href="#" class="btn-ver-mais" style="display: none">
        {{ t('catalogo.ver-mais') }} +
    </a>
</div>

@endsection