@extends('frontend.common.template')

@section('content')

<div class="catalogo catalogo-index">
    <div class="center">
        <div class="top-bar">
            <div>
                <h2>
                    {{ t('catalogo.resultado') }}
                    @if(request('codigo'))
                    &middot;
                    <strong>{{ request('codigo') }}</strong>
                    @endif
                </h2>

            </div>

            @include('frontend.catalogo._form-busca')
        </div>
    </div>

    <div class="center">
        <p class="resultados">
            @if(count($produtos) == 0)
            {{ t('catalogo.nenhum') }}
            @elseif(count($produtos) == 1)
            1 {{ t('catalogo.encontrado') }}
            @else
            {{ count($produtos) }} {{ t('catalogo.encontrados') }}
            @endif
        </p>
    </div>

    <div class="produtos">
        @foreach($produtos as $produto)
        <a href="{{ route('produtos.tipo', [$produto->tipo->slug, $produto->codigo]) }}">
            <div class="imagem" style="background-image:url('{{ prod_url('assets/img/produtos/capas/'.$produto->imagem_capa) }}')"></div>
            <div class="overlay">
                <span>
                    @if($produto->item)
                    {{ tobj($produto, 'item') }} • {{ $produto->codigo }}
                    @else
                    {{ $produto->codigo }}
                    @endif
                    @if(auth('profissional')->check() && auth('profissional')->user()->is_funcionario)
                    <br>{{ tobj($produto, 'titulo') }}
                    @if($produto->fornecedor)
                    <br>{{ $produto->fornecedor->titulo }}
                    @endif
                    @endif
                </span>
            </div>
            {{ tobj($produto, 'titulo') }}
        </a>
        @endforeach
    </div>
</div>

@endsection