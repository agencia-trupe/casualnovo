@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Designers /</small> Adicionar Designer</h2>
    </legend>

    {!! Form::open(['route' => 'painel.designers.store', 'files' => true]) !!}

        @include('painel.designers.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
