@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Designers
        <div class="btn-group pull-right">
            <a href="{{ route('painel.designers-frase.index') }}" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Editar Frase</a>
            <a href="{{ route('painel.designers.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Designer</a>
        </div>
    </h2>
</legend>

<div class="row" style="margin-bottom:20px">
    <div class="filtros">
        <h4 class="titulo">LOCAL:</h4>
        <a href="{{ route('painel.designers.index') }}" class="filtro todos {{ (!isset($_GET['local'])) ? 'active' : '' }}">TODOS</a>
        <a href="{{ route('painel.designers.index', ['local' => 'brasil']) }}" class="filtro {{ (isset($_GET['local']) && $_GET['local'] == 'brasil') ? 'active' : '' }}">Brasil</a>
        <a href="{{ route('painel.designers.index', ['local' => 'internacional']) }}" class="filtro {{ (isset($_GET['local']) && $_GET['local'] == 'internacional') ? 'active' : '' }}">Internacional</a>
    </div>
</div>

@if(!count($registros))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="designers">
    <thead>
        <tr>
            <th>Ordenar</th>
            <th>Nome</th>
            <th>Foto</th>
            <th>Imagens</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($registros as $registro)
        <tr class="tr-row" id="{{ $registro->id }}">
            <td>
                <a href="#" class="btn btn-info btn-sm btn-move">
                    <span class="glyphicon glyphicon-move"></span>
                </a>
            </td>
            <td>{{ $registro->nome }}</td>
            <td><img src="{{ asset('assets/img/designers/'.$registro->foto) }}" style="width: 100%; max-width:150px;" alt=""></td>
            <td><a href="{{ route('painel.designers.imagens.index', $registro->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Gerenciar
                </a></td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.designers.destroy', $registro->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.designers.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection