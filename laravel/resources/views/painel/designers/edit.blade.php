@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Designers /</small> Editar Designer</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.designers.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.designers.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
