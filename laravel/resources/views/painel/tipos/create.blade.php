@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / Tipos /</small> Adicionar Tipo</h2>
    </legend>

    {!! Form::open(['route' => 'painel.tipos.store', 'files' => true]) !!}

        @include('painel.tipos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
