@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('frase', 'Frase') !!}
    {!! Form::text('frase', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('frase_en', 'Frase [EN]') !!}
    {!! Form::text('frase_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('frase_es', 'Frase [ES]') !!}
    {!! Form::text('frase_es', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
