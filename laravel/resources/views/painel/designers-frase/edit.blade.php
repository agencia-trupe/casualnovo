@extends('painel.common.template')

@section('content')

    <a href="{{ route('painel.designers.index') }}" class="btn btn-sm btn-default">
        &larr; Voltar para Designers
    </a>

    <legend>
        <h2><small>Designers /</small> Frase</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.designers-frase.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.designers-frase.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
