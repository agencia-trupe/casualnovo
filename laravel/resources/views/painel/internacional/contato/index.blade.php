@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Informações de Contato</h2>
    </legend>

    {!! Form::model($contato, [
        'route'  => ['painel.internacional.contato.update', $contato->id],
        'method' => 'patch'])
    !!}

    @include('painel.internacional.contato.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
