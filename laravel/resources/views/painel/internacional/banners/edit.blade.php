@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Banners /</small> Editar Banner</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.internacional.banners.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.internacional.banners.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
