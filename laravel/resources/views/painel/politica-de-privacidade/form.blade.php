@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto', 'Texto - Política de Privacidade') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'blog']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_en', 'Texto EN - Política de Privacidade') !!}
    {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'blog']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_es', 'Texto ES - Política de Privacidade') !!}
    {!! Form::textarea('texto_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'blog']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}