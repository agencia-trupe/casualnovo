@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Endereços /</small> Editar Endereço</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.enderecos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.enderecos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
