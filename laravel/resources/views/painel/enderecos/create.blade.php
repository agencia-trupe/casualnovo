@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Endereços /</small> Adicionar Endereço</h2>
    </legend>

    {!! Form::open(['route' => 'painel.enderecos.store', 'files' => true]) !!}

        @include('painel.enderecos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
