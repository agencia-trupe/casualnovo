@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos /</small> Adicionar Produto</h2>
        <h3>
            <small>Fornecedor:</small> {{ $fornecedor->titulo }}<br>
            <small>Linha:</small> {{ $linha->titulo }}
        </h3>
    </legend>

    {!! Form::open(['route' => ['painel.fornecedores.linhas.produtos.store', $fornecedor->id, $linha->id], 'files' => true]) !!}

        @include('painel.produtos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
