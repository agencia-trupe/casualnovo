@include('painel.common.flash')

<div class="well">
    <div class="checkbox" style="margin:0">
        <label style="font-weight:bold">
            {!! Form::hidden('brasil', 0) !!}
            {!! Form::checkbox('brasil') !!}
            BRASIL
        </label>
    </div>
    <div class="checkbox" style="margin:0">
        <label style="font-weight:bold">
            {!! Form::hidden('internacional', 0) !!}
            {!! Form::checkbox('internacional') !!}
            INTERNACIONAL
        </label>
    </div>
</div>

@if(isset($registro))
<div class="well" style="padding-bottom:0">
    <div class="form-group">
        {!! Form::label('fornecedores_id', 'Alterar Fornecedor') !!}
        {!! Form::select('fornecedores_id', $fornecedores, null, ['class' => 'form-control handle-fornecedores', 'placeholder' => 'Selecione', 'required']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('linhas_id', 'Alterar Linha') !!}
        {!! Form::select('linhas_id', $linhas, null, ['class' => 'form-control handle-linhas', 'placeholder' => 'Selecione', 'required']) !!}
    </div>
</div>
@endif

<div class="well">
    <strong>Divisões</strong>
    <hr style="border-color:#ddd;margin:.5em 0 1em">

    @foreach(divisoes() as $divisao)
    <div class="checkbox" style="margin:.5em 0 0">
        <label>
            <input type="checkbox" name="divisao[]" value="{{ $divisao }}" @if(isset($registro) && str_contains($registro->divisao, $divisao) || (count(old('divisao')) && in_array($divisao, old('divisao')))) checked @endif>
            <span style="font-weight:bold">{{ ucfirst($divisao) }}</span>
        </label>
    </div>
    @endforeach
</div>

<div class="form-group">
    {!! Form::label('tipos_id', 'Tipo') !!}
    {!! Form::select('tipos_id', $tipos, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('origem', 'Origem') !!}
    {!! Form::select('origem', ['nacional' => 'Nacional', 'importado' => 'Importado'], null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="well">
    <strong>Publicar</strong>
    <hr style="border-color:#ddd;margin:.5em 0 1em">
    <div class="checkbox" style="margin:0 0 .5em">
        <label>
            {!! Form::hidden('publicar_site', 0) !!}
            {!! Form::checkbox('publicar_site', 1) !!}
            <span style="font-weight:bold">Publicar Site</span>
        </label>
    </div>
    <div class="checkbox" style="margin:0 0 1em">
        <label>
            {!! Form::hidden('publicar_catalogo', 0) !!}
            {!! Form::checkbox('publicar_catalogo', 1) !!}
            <span style="font-weight:bold">Publicar Catálogo</span>
        </label>
    </div>
    <div class="panel panel-default" style="margin:0">
        <div class="panel-body">
            O Fornecedor deste produto (<span class="text-info">{{ $fornecedor->titulo }}</span>) está publicado em:<br>

            @if($fornecedor->publicar_site == 1)
            <span class='text-success'><span class='glyphicon glyphicon-ok'></span> Site</span><br>
            @else
            <span class='text-danger'><span class='glyphicon glyphicon-remove'></span> Site</span><br>
            @endif

            @if($fornecedor->publicar_catalogo == 1)
            <span class='text-success'><span class='glyphicon glyphicon-ok'></span> Catálogo</span><br>
            @else
            <span class='text-danger'><span class='glyphicon glyphicon-remove'></span> Catálogo</span><br>
            @endif
        </div>
    </div>
</div>

<div class="well">
    <div class="checkbox" style="margin:0">
        <label>
            {!! Form::hidden('outlet', 0) !!}
            {!! Form::checkbox('outlet', 1) !!}
            <span style="font-weight:bold">Outlet</span>
        </label>
    </div>
</div>

<div class="well">
    <div class="checkbox" style="margin:0">
        <label>
            {!! Form::hidden('mostrar_em_listas', 0) !!}
            {!! Form::checkbox('mostrar_em_listas', 1) !!}
            <span style="font-weight:bold">Incluir Produto nas listagens de produtos aleatórias</span>
        </label>
    </div>
</div>

<div class="well">
    <div class="checkbox" style="margin:0">
        <label>
            {!! Form::hidden('exibir_3d', 0) !!}
            {!! Form::checkbox('exibir_3d', 1) !!}
            <span style="font-weight:bold">Exibir visualização 3D</span>
        </label>
    </div>
</div>

<hr>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_en', 'Título [EN]') !!}
    {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_es', 'Título [ES]') !!}
    {!! Form::text('titulo_es', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('item', 'Item/Peça (exibido no site - opcional)') !!}
    {!! Form::text('item', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('item_en', 'Item/Peça (exibido no site - opcional) [EN]') !!}
    {!! Form::text('item_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('item_es', 'Item/Peça (exibido no site - opcional) [ES]') !!}
    {!! Form::text('item_es', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('codigo', 'Código') !!}
    {!! Form::text('codigo', null, ['class' => 'form-control input-codigo']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_capa', 'Imagem Capa') !!}
    @if($submitText == 'Alterar')
    <img src="{{ prod_url('assets/img/produtos/capas/'.$registro->imagem_capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_capa', ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('descritivo', 'Descritivo') !!}
    {!! Form::textarea('descritivo', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="form-group">
    {!! Form::label('descritivo_en', 'Descritivo [EN]') !!}
    {!! Form::textarea('descritivo_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="form-group">
    {!! Form::label('descritivo_es', 'Descritivo [ES]') !!}
    {!! Form::textarea('descritivo_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="form-group">
    {!! Form::label('designer', 'Designer') !!}
    {!! Form::text('designer', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('dimensoes', 'Dimensões') !!}
    {!! Form::textarea('dimensoes', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="form-group">
    {!! Form::label('materiais', 'Materiais') !!}
    {!! Form::select('materiais[]', $materiais, isset($registro) ? $registro->materiais->pluck('id')->toArray() : null, ['class' => 'form-control multi-select', 'multiple' => true]) !!}
</div>

{!! Form::label('imagens', 'Imagens') !!}
<div class="well">
    <span class="btn btn-success btn-sm" style="position:relative;overflow:hidden;margin-bottom:10px;">
        <span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>
        Adicionar Imagens
        <input id="images-upload-produtos" data-url="{{ route('painel.produtos.imagens.upload') }}" type="file" name="imagem" id="imagem" multiple style="position:absolute;top:0;right:0;opacity:0;font-size:200px;cursor:pointer;">
    </span>

    <div class="progress progress-striped active">
        <div class="progress-bar" style="width: 0"></div>
    </div>

    <div class="alert alert-block alert-danger errors" style="display:none"></div>

    <div class="alert alert-info" style="display:inline-block;padding:10px 15px;">
        <small>
            <span class="glyphicon glyphicon-move" style="margin-right: 10px;"></span>
            Clique e arraste as imagens para ordená-las.
        </small>
    </div>

    <div id="imagens" class="clearfix no-sort">
        @if(count(old('imagens')))
        @foreach(old('imagens') as $imagem)
        @include('painel.produtos.imagem', ['imagem' => $imagem])
        @endforeach
        @elseif(!isset($registro) || !count($registro->imagens))
        <div class="alert alert-warning no-images" role="alert">Nenhuma imagem cadastrada.</div>
        @elseif(isset($registro))
        @foreach($registro->imagens as $imagem)
        @include('painel.produtos.imagem', ['imagem' => $imagem->imagem])
        @endforeach
        @endif
    </div>
</div>

{!! Form::hidden('origin', request('origin')) !!}

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ request('origin') ? request('origin') : route('painel.fornecedores.linhas.produtos.index', [$fornecedor->id, $linha->id]) }}" class="btn btn-default btn-voltar">Voltar</a>