<div class="imagem col-md-2 col-sm-3 col-xs-4" style="margin:5px 0;position:relative;padding:0 5px;" data-imagem="{{ $imagem }}">
    <img src="{{ prod_url('assets/img/produtos/capas/'.$imagem) }}" alt="" style="display:block;width:100%;height:auto;cursor:move;">
    <input type="hidden" name="imagens[]" value="{{ $imagem }}">
    <div class="btn-group btn-group-sm" style="position:absolute;bottom:8px;left:10px;">
        <button type="button" class="btn btn-danger btn-sm btn-delete-image-dom"><span class="glyphicon glyphicon-remove"></span></button>
    </div>
</div>
