@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.fornecedores.linhas.index', $fornecedor->id) }}" class="btn btn-default btn-sm">
        &larr; Voltar para Linhas
    </a>

    <legend>
        <h2>
            Produtos
        </h2>
        <h3>
            <small>Linha:</small> {{ $linha->titulo }}<br>
            <small>Fornecedor:</small> {{ $fornecedor->titulo }}
            <a href="{{ route('painel.fornecedores.linhas.produtos.create', [$fornecedor->id, $linha->id]) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Produto</a>
        </h3>
    </legend>


    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="produtos">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Tipo</th>
                <th>Divisões</th>
                <th>Título</th>
                <th>Código</th>
                <th>Capa</th>
                <th>Outlet</th>
                {{-- <th>Imagens</th> --}}
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td>{{ $registro->tipo ? $registro->tipo->titulo : '-' }}</td>
                <td>
                    <small>
                        @foreach(explode(',', $registro->divisao) as $divisao)
                            {{ ucfirst($divisao) }}<br>
                        @endforeach
                    </small>
                </td>
                <td>{{ $registro->titulo }}</td>
                <td>{{ $registro->codigo }}</td>
                <td><img src="{{ prod_url('assets/img/produtos/capas/'.$registro->imagem_capa) }}" style="width: 100%; max-width:80px;" alt=""></td>
                <td style="text-align:center">
                    <span class="glyphicon {{ $registro->outlet ? 'text-success glyphicon-ok' : 'text-danger glyphicon-remove' }}"></span>
                </td>
                {{-- <td><a href="{{ route('painel.fornecedores.linhas.produtos.imagens.index', [$fornecedor->id, $linha->id, $registro->id]) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Gerenciar
                </a></td> --}}
                <td class="crud-actions" style="width:185px">
                    {!! Form::open([
                        'route'  => ['painel.fornecedores.linhas.produtos.destroy', $fornecedor->id, $linha->id, $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.fornecedores.linhas.produtos.edit', [$fornecedor->id, $linha->id, $registro->id]) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
