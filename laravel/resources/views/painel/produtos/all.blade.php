@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Todos os Produtos
    </h2>
</legend>

<div class="row" style="margin-bottom:20px">
    <div class="col-sm-4">
        <form action="{{ route('painel.produtos.all') }}" method="GET" class="form-group" style="display:flex">
            <input type="text" name="busca" placeholder="busca (título ou código)" class="form-control" value="{{ request('busca') }}" required>
            <button type="submit" class="btn btn-md btn-warning">
                <span class="glyphicon glyphicon-search"></span>
            </button>
        </form>
    </div>
    <div class="col-sm-8 filtros">
        <h4 class="titulo">LOCAL:</h4>
        <a href="{{ route('painel.produtos.all') }}" class="filtro todos {{ (!isset($_GET['local'])) ? 'active' : '' }}">TODOS</a>
        <a href="{{ route('painel.produtos.all', ['local' => 'brasil']) }}" class="filtro {{ (isset($_GET['local']) && $_GET['local'] == 'brasil') ? 'active' : '' }}">Brasil</a>
        <a href="{{ route('painel.produtos.all', ['local' => 'internacional']) }}" class="filtro {{ (isset($_GET['local']) && $_GET['local'] == 'internacional') ? 'active' : '' }}">Internacional</a>
    </div>
</div>

@if(!count($registros))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info" style="font-size: 14px;">
    <thead>
        <tr>
            <th>Tipo</th>
            <th>Divisões</th>
            <th>Título</th>
            <th>Código</th>
            <th>Capa</th>
            <th>Outlet</th>
            <th>Imagens</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($registros as $registro)
        <tr class="tr-row" id="{{ $registro->id }}">
            <td>{{ $registro->tipo ? $registro->tipo->titulo : '-' }}</td>
            <td>
                <small>
                    @foreach(explode(',', $registro->divisao) as $divisao)
                    {{ ucfirst($divisao) }}<br>
                    @endforeach
                </small>
            </td>
            <td>{{ $registro->titulo }}</td>
            <td>{{ $registro->codigo }}</td>
            <td><img src="{{ prod_url('assets/img/produtos/capas/'.$registro->imagem_capa) }}" style="width: 100%; max-width:80px;" alt=""></td>
            <td style="text-align:center">
                <span class="glyphicon {{ $registro->outlet ? 'text-success glyphicon-ok' : 'text-danger glyphicon-remove' }}"></span>
            </td>
            <td><a href="{{ route('painel.fornecedores.linhas.produtos.imagens.index', [$registro->fornecedor->id, $registro->linha->id, $registro->id]) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Gerenciar
                </a></td>
            <td class="crud-actions" style="width:185px">
                {!! Form::open([
                'route' => ['painel.fornecedores.linhas.produtos.destroy', $registro->fornecedor->id, $registro->linha->id, $registro->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.fornecedores.linhas.produtos.edit', [$registro->fornecedor->id, $registro->linha->id, $registro->id, 'origin' => Request::fullUrl()]) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

{!! $registros->appends($_GET)->render() !!}
@endif

@endsection