@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos /</small> Editar Produto</h2>
        <h3>
            <small>Fornecedor:</small> {{ $fornecedor->titulo }}<br>
            <small>Linha:</small> {{ $linha->titulo }}
        </h3>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.fornecedores.linhas.produtos.update', $fornecedor->id, $linha->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.produtos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
