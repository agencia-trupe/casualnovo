<ul class="nav navbar-nav">
    <li class="dropdown @if(Tools::routeIs([
        'painel.banners*',
        'painel.home*',
        'painel.chamadas*',
        'painel.marcas*',
        'painel.perfil*',
        'painel.designers*',
    ])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Institucional
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.banners*')) class="active" @endif>
                <a href="{{ route('painel.banners.index') }}">Banners</a>
            </li>
            <li @if(Tools::routeIs('painel.home*')) class="active" @endif>
                <a href="{{ route('painel.home.index') }}">Home</a>
            </li>
            <li @if(Tools::routeIs('painel.chamadas*')) class="active" @endif>
                <a href="{{ route('painel.chamadas.index') }}">Chamadas</a>
            </li>
            <li class="divider"></li>
            <li @if(Tools::routeIs('painel.marcas*')) class="active" @endif>
                <a href="{{ route('painel.marcas.index') }}">Marcas</a>
            </li>
            <li @if(Tools::routeIs('painel.perfil*')) class="active" @endif>
                <a href="{{ route('painel.perfil.index') }}">Perfil</a>
            </li>
            <li @if(Tools::routeIs('painel.designers*')) class="active" @endif>
                <a href="{{ route('painel.designers.index') }}">Designers</a>
            </li>
        </ul>
    </li>
    <li class="dropdown @if(Tools::routeIs([
        'painel.fornecedores*',
        'painel.produtos*',
        'painel.tipos*',
        'painel.produtos-materiais*',
    ])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Catálogo
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.fornecedores*')) class="active" @endif>
                <a href="{{ route('painel.fornecedores.index') }}">Fornecedores</a>
            </li>
            <li @if(Tools::routeIs('painel.produtos.all')) class="active" @endif>
                <a href="{{ route('painel.produtos.all') }}">Todos os Produtos</a>
            </li>
            <li class="divider"></li>
            <li @if(Tools::routeIs('painel.tipos*')) class="active" @endif>
                <a href="{{ route('painel.tipos.index') }}">Tipos de Produtos</a>
            </li>
            <li @if(Tools::routeIs('painel.produtos-materiais*')) class="active" @endif>
                <a href="{{ route('painel.produtos-materiais.index') }}">Materiais de Produtos</a>
            </li>
        </ul>
    </li>
    <li @if(Tools::routeIs('painel.blog*')) class="active" @endif>
        <a href="{{ route('painel.blog.index') }}">Blog</a>
    </li>
    <li class="dropdown @if(Tools::routeIs(['painel.contato*', 'painel.enderecos*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.contato.index')) class="active" @endif>
                <a href="{{ route('painel.contato.index') }}">Informações de Contato</a>
            </li>
            <li @if(Tools::routeIs('painel.enderecos*')) class="active" @endif>
                <a href="{{ route('painel.enderecos.index') }}">Endereços</a>
            </li>
            <li @if(Tools::routeIs('painel.contato.recebidos*')) class="active" @endif>
                <a href="{{ route('painel.contato.recebidos.index') }}">
                    Contatos Recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li>
    <li class="dropdown @if(Tools::routeIs([
        'painel.area-profissional.imagem-de-capa*',
        'painel.area-profissional.e-mail-avisos*',
        'painel.area-profissional.tela-inicial*',
        'painel.area-profissional.usuarios*',
        'painel.area-profissional.orcamentos*',
    ])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Área Profissional
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.area-profissional.imagem-de-capa*')) class="active" @endif>
                <a href="{{ route('painel.area-profissional.imagem-de-capa.index') }}">Imagem de Capa</a>
            </li>
            <li @if(Tools::routeIs('painel.area-profissional.e-mail-avisos*')) class="active" @endif>
                <a href="{{ route('painel.area-profissional.e-mail-avisos.index') }}">E-mail para Avisos</a>
            </li>
            <li @if(Tools::routeIs('painel.area-profissional.tela-inicial*')) class="active" @endif>
                <a href="{{ route('painel.area-profissional.tela-inicial.index') }}">Tela Inicial</a>
            </li>
            <li class="divider"></li>
            <li @if(Tools::routeIs('painel.area-profissional.usuarios*')) class="active" @endif>
                <a href="{{ route('painel.area-profissional.usuarios.index') }}">Usuários</a>
            </li>
            <li @if(Tools::routeIs('painel.area-profissional.orcamentos*')) class="active" @endif>
                <a href="{{ route('painel.area-profissional.orcamentos.index') }}">Orçamentos</a>
            </li>
        </ul>
    </li>

    <li class="dropdown @if(Tools::routeIs([
        'painel.internacional.contato*',
        'painel.internacional.configuracoes*',
        'painel.internacional.banners*',
        'painel.internacional.home*',
        'painel.internacional.perfil*'
    ])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Internacional
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.internacional.contato.index')) class="active" @endif>
                <a href="{{ route('painel.internacional.contato.index') }}">Informações de Contato</a>
            </li>
            <li @if(Tools::routeIs('painel.internacional.configuracoes.index')) class="active" @endif>
                <a href="{{ route('painel.internacional.configuracoes.index') }}">Configurações</a>
            </li>
            <li @if(Tools::routeIs('painel.internacional.banners*')) class="active" @endif>
                <a href="{{ route('painel.internacional.banners.index') }}">Banners</a>
            </li>
            <li @if(Tools::routeIs('painel.internacional.home*')) class="active" @endif>
                <a href="{{ route('painel.internacional.home.index') }}">Home</a>
            </li>
            <li @if(Tools::routeIs('painel.internacional.perfil*')) class="active" @endif>
                <a href="{{ route('painel.internacional.perfil.index') }}">Perfil</a>
            </li>
        </ul>
    </li>
</ul>