@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / Materiais /</small> Editar Material</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtos-materiais.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.produtos-materiais.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
