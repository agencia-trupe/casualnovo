@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / Materiais /</small> Adicionar Material</h2>
    </legend>

    {!! Form::open(['route' => 'painel.produtos-materiais.store', 'files' => true]) !!}

        @include('painel.produtos-materiais.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
