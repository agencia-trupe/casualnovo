@extends('painel.common.template')

@section('content')

    <h1>Bem Vindo(a),</h1>
    <br>
    <p>
        Pelo painel administrativo da
        <a href="http://www.trupe.net" target="_blank">Trupe</a>
        você pode controlar as principais funções e conteúdo do site.
    </p>
    <p>
        Em caso de dúvidas entrar em contato:
        <a href="mailto:contato@trupe.net">contato@trupe.net</a>
    </p>

@endsection
