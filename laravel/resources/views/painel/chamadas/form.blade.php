@include('painel.common.flash')

<div class="well">
    <div class="checkbox" style="margin:0">
        <label style="font-weight:bold">
            {!! Form::hidden('brasil', 0) !!}
            {!! Form::checkbox('brasil') !!}
            BRASIL
        </label>
    </div>
    <div class="checkbox" style="margin:0">
        <label style="font-weight:bold">
            {!! Form::hidden('internacional', 0) !!}
            {!! Form::checkbox('internacional') !!}
            INTERNACIONAL
        </label>
    </div>
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/chamadas/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('subtitulo', 'Subtítulo') !!}
            {!! Form::text('subtitulo', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('titulo', 'Título') !!}
            {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('subtitulo_en', 'Subtítulo [EN]') !!}
            {!! Form::text('subtitulo_en', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('titulo_en', 'Título [EN]') !!}
            {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('subtitulo_es', 'Subtítulo [ES]') !!}
            {!! Form::text('subtitulo_es', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('titulo_es', 'Título [ES]') !!}
            {!! Form::text('titulo_es', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('link', 'Link') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.chamadas.index') }}" class="btn btn-default btn-voltar">Voltar</a>