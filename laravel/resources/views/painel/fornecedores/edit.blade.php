@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Fornecedores /</small> Editar Fornecedor</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.fornecedores.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.fornecedores.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
