@include('painel.common.flash')

<div class="well">
    <div class="checkbox" style="margin:0">
        <label style="font-weight:bold">
            {!! Form::hidden('brasil', 0) !!}
            {!! Form::checkbox('brasil') !!}
            BRASIL
        </label>
    </div>
    <div class="checkbox" style="margin:0">
        <label style="font-weight:bold">
            {!! Form::hidden('internacional', 0) !!}
            {!! Form::checkbox('internacional') !!}
            INTERNACIONAL
        </label>
    </div>
</div>

<div class="well">
    <strong>Divisões</strong>
    <hr style="border-color:#ddd;margin:.5em 0 1em">

    @foreach(divisoes() as $divisao)
    <div class="checkbox" style="margin:.5em 0 0">
        <label>
            <input type="checkbox" name="divisao[]" value="{{ $divisao }}" @if(isset($registro) && str_contains($registro->divisao, $divisao) || (count(old('divisao')) && in_array($divisao, old('divisao')))) checked @endif>
            <span style="font-weight:bold">{{ ucfirst($divisao) }}</span>
        </label>
    </div>
    @endforeach
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/fornecedores/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('obs', 'Observações') !!}
    {!! Form::textarea('obs', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="well">
    <strong>Publicar</strong>
    <hr style="border-color:#ddd;margin:.5em 0 1em">
    <div class="checkbox" style="margin:0 0 .5em">
        <label>
            {!! Form::hidden('publicar_site', 0) !!}
            {!! Form::checkbox('publicar_site', 1) !!}
            <span style="font-weight:bold">Publicar Site</span>
        </label>
    </div>
    <div class="checkbox" style="margin:0">
        <label>
            {!! Form::hidden('publicar_catalogo', 0) !!}
            {!! Form::checkbox('publicar_catalogo', 1) !!}
            <span style="font-weight:bold">Publicar Catálogo</span>
        </label>
    </div>
</div>


{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.fornecedores.index') }}" class="btn btn-default btn-voltar">Voltar</a>