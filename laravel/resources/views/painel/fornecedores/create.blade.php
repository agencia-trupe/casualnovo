@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Fornecedores /</small> Adicionar Fornecedor</h2>
    </legend>

    {!! Form::open(['route' => 'painel.fornecedores.store', 'files' => true]) !!}

        @include('painel.fornecedores.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
