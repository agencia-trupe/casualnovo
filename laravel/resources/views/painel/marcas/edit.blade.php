@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Marcas /</small> Editar Marca</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.marcas.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.marcas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
