@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Marcas /</small> Adicionar Marca</h2>
    </legend>

    {!! Form::open(['route' => 'painel.marcas.store', 'files' => true]) !!}

        @include('painel.marcas.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
