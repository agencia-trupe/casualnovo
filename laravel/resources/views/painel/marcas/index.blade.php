@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Marcas
        <a href="{{ route('painel.marcas.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Marca</a>
    </h2>
</legend>

<div class="row" style="margin-bottom:20px">
    <div class="filtros">
        <h4 class="titulo">LOCAL:</h4>
        <a href="{{ route('painel.marcas.index') }}" class="filtro todos {{ (!isset($_GET['local'])) ? 'active' : '' }}">TODOS</a>
        <a href="{{ route('painel.marcas.index', ['local' => 'brasil']) }}" class="filtro {{ (isset($_GET['local']) && $_GET['local'] == 'brasil') ? 'active' : '' }}">Brasil</a>
        <a href="{{ route('painel.marcas.index', ['local' => 'internacional']) }}" class="filtro {{ (isset($_GET['local']) && $_GET['local'] == 'internacional') ? 'active' : '' }}">Internacional</a>
    </div>
</div>

@if(!count($registros))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="marcas">
    <thead>
        <tr>
            <th>Ordenar</th>
            <th>Nome</th>
            <th>Divisões</th>
            <th>Imagem</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($registros as $registro)
        <tr class="tr-row" id="{{ $registro->id }}">
            <td>
                <a href="#" class="btn btn-info btn-sm btn-move">
                    <span class="glyphicon glyphicon-move"></span>
                </a>
            </td>
            <td>{{ $registro->nome }}</td>
            <td>
                <small>
                    @foreach(explode(',', $registro->divisao) as $divisao)
                    {{ ucfirst($divisao) }}<br>
                    @endforeach
                </small>
            </td>
            <td><img src="{{ asset('assets/img/marcas/'.$registro->imagem) }}" alt=""></td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.marcas.destroy', $registro->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.marcas.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection