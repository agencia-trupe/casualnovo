@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Linhas /</small> Editar Linha</h2>
        <h3>
            <small>Fornecedor:</small> {{ $fornecedor->titulo }}
        </h3>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.fornecedores.linhas.update', $fornecedor->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.linhas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
