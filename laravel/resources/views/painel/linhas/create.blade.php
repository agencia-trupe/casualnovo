@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Linhas /</small> Adicionar Linha</h2>
        <h3>
            <small>Fornecedor:</small> {{ $fornecedor->titulo }}
        </h3>
    </legend>

    {!! Form::open(['route' => ['painel.fornecedores.linhas.store', $fornecedor->id], 'files' => true]) !!}

        @include('painel.linhas.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
