@include('painel.common.flash')

<div class="well">
    <div class="checkbox" style="margin:0">
        <label style="font-weight:bold">
            {!! Form::hidden('brasil', 0) !!}
            {!! Form::checkbox('brasil') !!}
            BRASIL
        </label>
    </div>
    <div class="checkbox" style="margin:0">
        <label style="font-weight:bold">
            {!! Form::hidden('internacional', 0) !!}
            {!! Form::checkbox('internacional') !!}
            INTERNACIONAL
        </label>
    </div>
</div>

<div class="form-group">
    {!! Form::label('blog_categorias_id', 'Categoria') !!}
    {!! Form::select('blog_categorias_id', $categorias, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo', 'Título') !!}
            {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo_en', 'Título [EN]') !!}
            {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo_es', 'Título [ES]') !!}
            {!! Form::text('titulo_es', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

<div class="form-group">
    {!! Form::label('data', 'Data') !!}
    {!! Form::text('data', null, ['class' => 'form-control datepicker']) !!}
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Imagem de Capa') !!}
    @if($submitText == 'Alterar' && $registro->capa)
    <img src="{{ url('assets/images/blog/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('tags', 'Tags') !!}
    {!! Form::text('tags', isset($registro) ? $registro->tags->implode('titulo', ',') : null, ['class' => 'tagsinput', 'data-existing-tags' => $tags]) !!}
    <div class="alert alert-info" style="margin-top:6px">As tags podem ser inseridas com o <strong>ENTER</strong> ou separadas por <strong>vírgula</strong>.</div>
</div>

<hr>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'blog']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_en', 'Texto [EN]') !!}
    {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'blog']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_es', 'Texto [ES]') !!}
    {!! Form::textarea('texto_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'blog']) !!}
</div>

<hr>

<div class="well">
    <div class="checkbox" style="margin:0">
        <label>
            {!! Form::hidden('publicar', 0) !!}
            {!! Form::checkbox('publicar', 1) !!}
            <span style="font-weight:bold">Publicar</span>
        </label>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.blog.index') }}" class="btn btn-default btn-voltar">Voltar</a>