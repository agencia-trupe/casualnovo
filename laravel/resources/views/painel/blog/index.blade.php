@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Blog
        <a href="{{ route('painel.blog.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Post</a>
    </h2>
</legend>

<div class="row" style="margin-bottom:20px">
    <div class="form-group col-sm-4">
        {!! Form::select('filtro', $categorias, Request::get('filtro'), ['class' => 'form-control', 'id' => 'filtro-select', 'placeholder' => 'Todas as Categorias', 'data-route' => 'painel/blog']) !!}
    </div>
    <div class="col-sm-4" style="padding-left:0">
        <a href="{{ route('painel.blog.categorias.index') }}" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-edit" style="margin-right:10px;"></span><small>Editar Categorias</small></a>
    </div>
</div>

<div class="row" style="margin-bottom:20px">
    <div class="filtros">
        <h4 class="titulo">LOCAL:</h4>
        <a href="{{ route('painel.blog.index') }}" class="filtro todos {{ (!isset($_GET['local'])) ? 'active' : '' }}">TODOS</a>
        <a href="{{ route('painel.blog.index', ['local' => 'brasil']) }}" class="filtro {{ (isset($_GET['local']) && $_GET['local'] == 'brasil') ? 'active' : '' }}">Brasil</a>
        <a href="{{ route('painel.blog.index', ['local' => 'internacional']) }}" class="filtro {{ (isset($_GET['local']) && $_GET['local'] == 'internacional') ? 'active' : '' }}">Internacional</a>
    </div>
</div>

@if(!count($registros))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info">
    <thead>
        <tr>
            <th>Publicado</th>
            <th>Título</th>
            <th>Data</th>
            <th>Categoria</th>
            <th>Comentários</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($registros as $registro)
        <tr class="tr-row" id="{{ $registro->id }}">
            <td style="text-align:center">
                <span class="glyphicon {{ $registro->publicar ? 'text-success glyphicon-ok' : 'text-danger glyphicon-remove' }}"></span>
            </td>
            <td>{{ $registro->titulo }}</td>
            <td>{{ $registro->data }}</td>
            <td>{{ $registro->categoria->titulo }}</td>
            <td>
                <a href="{{ route('painel.blog.comentarios.index', $registro->id) }}" class="btn btn-sm btn-info">
                    <span class="glyphicon glyphicon-th-list" style="margin-right:10px"></span>
                    Gerenciar
                </a>
            </td>
            <td class="crud-actions" style="width:185px">
                {!! Form::open([
                'route' => ['painel.blog.destroy', $registro->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.blog.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@if($registros instanceof \Illuminate\Pagination\LengthAwarePaginator)
{!! $registros->appends($_GET)->render() !!}
@endif
@endif

@endsection