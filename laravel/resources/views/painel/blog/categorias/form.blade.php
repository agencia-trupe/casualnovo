@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_en', 'Título [EN]') !!}
    {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_es', 'Título [ES]') !!}
    {!! Form::text('titulo_es', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.blog.categorias.index') }}" class="btn btn-default btn-voltar">Voltar</a>
