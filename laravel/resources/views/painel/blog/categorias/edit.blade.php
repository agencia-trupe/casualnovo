@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Blog /</small> Editar Categoria</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.blog.categorias.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.blog.categorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
