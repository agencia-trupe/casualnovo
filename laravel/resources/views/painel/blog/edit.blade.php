@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Blog /</small> Editar Post</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.blog.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.blog.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
