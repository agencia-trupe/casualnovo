@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            <small>Blog / {{ $post->titulo }} /</small> Visualizar Comentário
        </h2>
    </legend>

    <div class="form-group">
        <label>Autor</label>
        <div class="well">{{ $registro->autor }}</div>
    </div>

    <div class="form-group">
        <label>E-mail</label>
        <div class="well">
            <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $registro->email }}" style="margin-right:5px;border:0;transition:background .3s">
                <span class="glyphicon glyphicon-copy"></span>
            </button>
            {{ $registro->email }}
        </div>
    </div>

    <div class="form-group">
        <label>Data</label>
        <div class="well">{{ $registro->created_at->format('d/m/Y') }}</div>
    </div>

    <div class="form-group">
        <label>Texto</label>
        <div class="well">{{ $registro->texto }}</div>
    </div>

    {!! Form::model($registro, [
        'route'  => ['painel.blog.comentarios.update', $post->id, $registro->id],
        'method' => 'patch'
    ]) !!}

    <div class="well">
        <div class="checkbox" style="margin:0">
            <label>
                {!! Form::hidden('aprovado', 0) !!}
                {!! Form::checkbox('aprovado', 1) !!}
                <span style="font-weight:bold">Aprovado</span>
            </label>
        </div>
    </div>

    {!! Form::submit('Alterar', ['class' => 'btn btn-success']) !!}
    <a href="{{ route('painel.blog.comentarios.index', $post) }}" class="btn btn-default btn-voltar">Voltar</a>

    {!! Form::close() !!}

@stop
