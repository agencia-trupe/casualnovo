@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.blog.index') }}" title="Voltar para Blog" class="btn btn-sm btn-default">
        &larr; Voltar para Blog
    </a>

    <legend>
        <h2>
            <small>Blog / {{ $post->titulo }} /</small> Comentários
        </h2>
    </legend>


    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Aprovado</th>
                <th>Autor</th>
                <th>E-mail</th>
                <th>Data</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td style="text-align:center">
                    <span class="glyphicon {{ $registro->aprovado ? 'text-success glyphicon-ok' : 'text-danger glyphicon-remove' }}"></span>
                </td>
                <td>{{ $registro->autor }}</td>
                <td>
                    <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $registro->email }}" style="margin-right:5px;border:0;transition:background .3s">
                        <span class="glyphicon glyphicon-copy"></span>
                    </button>
                    {{ $registro->email }}
                </td>
                <td>{{ $registro->created_at->format('d/m/Y') }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.blog.comentarios.destroy', $post->id, $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.blog.comentarios.show', [$post->id, $registro->id]) }}" class="btn btn-default btn-sm pull-left">
                            <span class="glyphicon glyphicon-align-left" style="margin-right:10px;"></span>Visualizar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
