@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            <small>Área Profissional / </small> Orçamento
        </h2>
    </legend>

    <div class="form-group">
        <label>Usuário</label>
        <div class="well">
            @if($orcamento->usuario)
                <td>
                    <a href="{{ route('painel.area-profissional.usuarios.show', $orcamento->usuario->id) }}">
                        {{ $orcamento->usuario->nome }}
                    </a>
                </td>
            @else
                <td>Usuário não encontrado</td>
            @endif
        </div>
    </div>

    <div class="form-group">
        <label>Data</label>
        <div class="well">{{ $orcamento->data }}</div>
    </div>

    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <th>Imagem</th>
            <th>Produto</th>
            <th>Quantidade</th>
        </thead>

        <tbody>
        @foreach($orcamento->itens as $item)
            <tr>
                <td style="width:96px">
                    <img src="{{ prod_url('assets/img/produtos/capas/'.$item->imagem) }}" style="width: 100%; max-width:80px;" alt="">
                </td>
                <td>
                    @if($item->produto)
                    <a href="{{ route('produtos.tipo', [$item->produto->tipo->slug, $item->produto->codigo]) }}" style="text-decoration:none" target="_blank">
                    @endif
                    <span style="font-size:1.1em;font-weight:bold">{{ $item->codigo }}</span>
                    <small>
                        @foreach(explode(',', $item->divisao) as $divisao)
                            <span style="display:block;text-transform:uppercase">{{ t('nav.'.$divisao) }}</span>
                        @endforeach
                    </small>
                    @if($item->produto)
                    </a>
                    @endif
                </td>
                <td>{{ $item->quantidade }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    @if($orcamento->observacoes)
    <div class="form-group">
        <label>Observações</label>
        <div class="well">{!! nl2br($orcamento->observacoes) !!}</div>
    </div>
    @endif

    <a href="{{ route('painel.area-profissional.orcamentos.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@endsection
