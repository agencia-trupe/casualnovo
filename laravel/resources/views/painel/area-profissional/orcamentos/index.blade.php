@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            <small>Área Profissional /</small> Orçamentos
        </h2>
    </legend>

    @if(!count($orcamentos))
    <div class="alert alert-warning" role="alert">Nenhum orçamento encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Usuário</th>
                <th>Data</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($orcamentos as $orcamento)
            <tr class="tr-row">
                @if($orcamento->usuario)
                    <td>
                        <a href="{{ route('painel.area-profissional.usuarios.show', $orcamento->usuario->id) }}">
                            {{ $orcamento->usuario->nome }}
                        </a>
                    </td>
                @else
                    <td>Usuário não encontrado</td>
                @endif
                <td>{{ $orcamento->data }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.area-profissional.orcamentos.destroy', $orcamento->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.area-profissional.orcamentos.show', $orcamento->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-align-left" style="margin-right:10px;"></span>Visualizar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

    {!! $orcamentos->render() !!}

@endsection
