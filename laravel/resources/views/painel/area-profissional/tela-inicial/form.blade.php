@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto', 'Texto') !!}
            {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>

        <div class="well form-group">
            {!! Form::label('imagem', 'Imagem') !!}
            @if($registro->imagem)
            <img src="{{ url('assets/img/tela-inicial/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_en', 'Texto [EN]') !!}
            {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>

        <div class="well form-group">
            {!! Form::label('imagem_en', 'Imagem [EN]') !!}
            @if($registro->imagem_en)
            <img src="{{ url('assets/img/tela-inicial/'.$registro->imagem_en) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_en', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>


{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
