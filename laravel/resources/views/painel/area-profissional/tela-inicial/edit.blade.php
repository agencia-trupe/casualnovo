@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            <small>Área Profissional /</small> Tela Inicial
        </h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.area-profissional.tela-inicial.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.area-profissional.tela-inicial.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
