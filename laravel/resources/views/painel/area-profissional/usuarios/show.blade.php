@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            <small>Área Profissional / Usuários /</small> {{ $usuario->nome }}
        </h2>
    </legend>

    <div class="form-group">
        <label>Nome</label>
        <div class="well">{{ $usuario->nome }}</div>
    </div>

    <div class="form-group">
        <label>E-mail</label>
        <div class="well">
            <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $usuario->email }}" style="margin-right:5px;border:0;transition:background .3s">
                <span class="glyphicon glyphicon-copy"></span>
            </button>
            {{ $usuario->email }}
        </div>
    </div>

    <div class="form-group">
        <label>CPF</label>
        <div class="well">{{ $usuario->cpf ?: '-' }}</div>
    </div>

    <div class="form-group">
        <label>Telefone</label>
        <div class="well">{{ $usuario->telefone }}</div>
    </div>

    <div class="form-group">
        <label>Empresa</label>
        <div class="well">{{ $usuario->empresa }}</div>
    </div>

    <div class="form-group">
        <label>Cargo</label>
        <div class="well">{{ $usuario->cargo }}</div>
    </div>

    <div class="form-group">
        <label>País</label>
        <div class="well">{{ $usuario->pais }}</div>
    </div>

    <div class="form-group">
        <label>CEP</label>
        <div class="well">{{ $usuario->cep }}</div>
    </div>

    <div class="form-group">
        <label>Endereço</label>
        <div class="well">{{ $usuario->endereco }}</div>
    </div>

    <div class="form-group">
        <label>Número</label>
        <div class="well">{{ $usuario->numero }}</div>
    </div>

    <div class="form-group">
        <label>Complemento</label>
        <div class="well">{{ $usuario->complemento ?: '-' }}</div>
    </div>

    <div class="form-group">
        <label>Cidade</label>
        <div class="well">{{ $usuario->cidade }}</div>
    </div>

    <div class="form-group">
        <label>UF</label>
        <div class="well">{{ $usuario->estado }}</div>
    </div>

    <hr>

    {!! Form::model($usuario, [
        'route' => ['painel.area-profissional.usuarios.update', $usuario->id],
        'method' => 'patch',
    ]) !!}

        <div class="row">
            <div class="col-md-6">
                <div class="well">
                    <div class="checkbox" style="margin:0">
                        <label>
                            {!! Form::hidden('is_funcionario', 0) !!}
                            {!! Form::checkbox('is_funcionario', 1) !!}
                            <span style="font-weight:bold">Cadastro de Funcionário</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="well">
                    <div class="checkbox" style="margin:0">
                        <label>
                            {!! Form::hidden('bloqueado', 0) !!}
                            {!! Form::checkbox('bloqueado', 1) !!}
                            <span style="font-weight:bold">Bloquear Usuário</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>

        {!! Form::submit('Alterar', ['class' => 'btn btn-success']) !!}
        <a href="{{ route('painel.area-profissional.usuarios.index') }}" class="btn btn-default btn-voltar">Voltar</a>

    {!! Form::close() !!}

@endsection
