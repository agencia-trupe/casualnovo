@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            <small>Área Profissional /</small> Usuários
            <a href="{{ route('painel.area-profissional.usuarios.newsletter') }}" class="btn btn-info btn-sm pull-right"><span class="glyphicon glyphicon-download-alt" style="margin-right:10px;"></span>Exportar CSV Newsletter</a>
        </h2>
    </legend>

    <div class="row" style="margin-bottom:20px">
        <div class="col-sm-4">
            <form action="{{ route('painel.area-profissional.usuarios.index') }}" method="GET" class="form-group" style="display:flex">
                <input type="text" name="busca" placeholder="buscar usuário" class="form-control" value="{{ request('busca') }}">
                <button type="submit" class="btn btn-md btn-warning">
                    <span class="glyphicon glyphicon-search"></span>
                </button>
            </form>
        </div>
    </div>

    @if(!count($usuarios))
    <div class="alert alert-warning" role="alert">Nenhum usuário encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="banners">
        <thead>
            <tr>
                <th>Usuário</th>
                <th>E-mail</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($usuarios as $usuario)
            <tr class="tr-row">
                <td>
                    @if($usuario->bloqueado)
                    <div class="label label-danger">Bloqueado</div>
                    @endif
                    @if($usuario->is_funcionario)
                    <div class="label label-success">Funcionário</div>
                    @endif
                    <div>{{ $usuario->nome }}</div>
                </td>
                <td>{{ $usuario->email }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.area-profissional.usuarios.destroy', $usuario->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.area-profissional.usuarios.show', $usuario->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-align-left" style="margin-right:10px;"></span>Visualizar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

    {!! $usuarios->appends($_GET)->render() !!}

@endsection
