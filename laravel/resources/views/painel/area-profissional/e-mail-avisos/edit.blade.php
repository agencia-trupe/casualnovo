@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            <small>Área Profissional /</small> E-mail para Avisos
        </h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.area-profissional.e-mail-avisos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.area-profissional.e-mail-avisos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
