@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            <small>Área Profissional /</small> Imagem de Capa
        </h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.area-profissional.imagem-de-capa.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.area-profissional.imagem-de-capa.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
