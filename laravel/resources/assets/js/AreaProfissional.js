import cep from 'cep-promise';

export default function() {
    $('input[name=cpf]').inputmask('999.999.999-99');
    $('input[name=cep]').inputmask('99999-999');

    $('input[name=cep]').on('blur', function() {
        const inputCep = $(this).val().replace(/[^0-9]+/g, '');

        if (inputCep.length !== 8) return;

        $('input[name=endereco], input[name=cidade], input[name=estado]')
            .val('...')
            .prop('disabled', true);

        cep($(this).val())
            .then(({
                street,
                city,
                state,
            }) => {
                $('input[name=endereco]').val(street);
                $('input[name=cidade]').val(city);
                $('input[name=estado]').val(state);
            })
            .catch(() => {
                $('input[name=endereco], input[name=cidade], input[name=estado]')
                    .val('');
            })
            .finally(() => {
                $('input[name=endereco], input[name=cidade], input[name=estado]')
                    .prop('disabled', false);
            });
    });

    $('.btn-adicionar-orcamento').click(function(event) {
        event.preventDefault();

        var _this = $(this);

        if (_this.hasClass('active')) {
            location.href = _this.data('list-url');
        }

        if (_this.hasClass('sending')) return;

        _this.addClass('sending');

        $.post(_this.data('add-url')).done((data) => {
            _this.addClass('active');

            var lingueta = $('.indicador-orcamento');

            var subtitulo = lingueta.find('.subtitulo');
            subtitulo.html(subtitulo.data('template').replace(':num', data.length));

            if (lingueta.is(':hidden')) {
                lingueta.addClass('closed').show().removeClass('closed');
            }
        }).fail((data) => {
            var erro = data.responseJSON
                ? data.responseJSON
                : 'Erro interno do servidor.';
            console.log('ERRO: ' + erro);
        }).always(() => {
            _this.removeClass('sending');
        });
    });

    $('.indicador-orcamento').click(function() {
        var $el = $(this);

        if ($el.hasClass('closed')) {
            $el.removeClass('closed');
        } else {
            location.href = $el.data('url');
        }
    });

    $('.indicador-orcamento .fechar').click(function(e) {
        e.stopPropagation();
        $(this).parent().addClass('closed');
    });
}
