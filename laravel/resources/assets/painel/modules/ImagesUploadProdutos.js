export default function ImagesUpload() {
    var $wrapper = $('#images-upload-produtos');
    var errors;

    $wrapper.fileupload({
        dataType: 'json',
        url: $wrapper.data('url'),
        type: 'POST',
        formData: [],
        start: function(e) {
            if ($('.no-images').length) $('.no-images').fadeOut();

            if ($('.errors').length) {
                errors = [];
                $('.errors').fadeOut().html('');
            }
        },
        done: function (e, data) {
            $('#imagens').append($(data.result.body).hide().addClass('new'))
                         .sortable('refresh');
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);

            $('.progress-bar').css(
                'width',
                progress + '%'
            );
        },
        stop: function() {
            $('.progress-bar').css('width', 0);

            $('#imagens').find('.imagem').appendTo($('#imagens'));

            $('#imagens .imagem.new').each(function(i) {
                $(this).delay((i++) * 400).fadeIn(300);
            });

            $('.imagem').removeClass('new');

            if (errors.length) {
                errors.forEach(function(message) {
                    $('.errors').append(message + '<br>');
                });
                $('.errors').fadeIn();
            }
        },
        fail: function(e, data) {
            var status       = data.jqXHR.status,
                errorMessage = (status == '422' ? 'O arquivo deve ser uma imagem.' : 'Erro interno do servidor.'),
                response     = 'Ocorreu um erro ao enviar o arquivo ' + data.files[0].name + ': ' + errorMessage;

            errors.push(response);
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

    $('body').on('click', '.btn-delete-image-dom', function(event) {
        event.preventDefault();

        $(this).parent().parent().remove();
    });
};
