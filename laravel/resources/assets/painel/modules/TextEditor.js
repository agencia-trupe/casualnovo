const config = {
    padrao: {
        toolbar: [['Bold', 'Italic']]
    },

    perfil: {
        toolbar: [['Format'], ['Bold', 'Italic']],
        format_tags: 'h3;p'
    },

    clean: {
        toolbar: [],
        removePlugins: 'toolbar,elementspath',
    },

    cleanBr: {
        toolbar: [],
        removePlugins: 'toolbar,elementspath',
        enterMode: CKEDITOR.ENTER_BR
    },

    blog: {
        removeButtons: '',
        toolbar: [
            ['Bold', 'Italic', 'Underline', 'Subscript', 'Superscript'],
            ['NumberedList', 'BulletedList'],
            ['Link', 'Unlink'],
            ['InjectImage'],
            ['TextColor'],
            ['FontSize']
        ],
        height: 450,
        format_tags: 'h1;h2;h3;p'
    }
};

export default function TextEditor() {
    CKEDITOR.config.language = 'pt-br';
    CKEDITOR.config.uiColor = '#dce4ec';
    CKEDITOR.config.contentsCss = [
        $('base').attr('href') + '/assets/ckeditor.css',
        CKEDITOR.config.contentsCss
    ];
    CKEDITOR.config.removePlugins = 'elementspath';
    CKEDITOR.config.resize_enabled = false;
    CKEDITOR.plugins.addExternal('injectimage', $('base').attr('href') + '/assets/injectimage/plugin.js');
    CKEDITOR.config.allowedContent = true;
    CKEDITOR.config.extraPlugins = 'injectimage,colorbutton,font';

    $('.ckeditor').each(function (i, obj) {
        CKEDITOR.replace(obj.id, config[obj.dataset.editor]);
    });
};
