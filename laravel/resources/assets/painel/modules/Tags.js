export default function Tags() {
    const $tagsInput = $('.tagsinput');

    if (! $tagsInput.length) return;

    const substringMatcher = function(strs) {
        return function findMatches(q, cb) {
            var matches, substrRegex;

            matches = [];
            substrRegex = new RegExp(q, 'i');

            $.each(strs, function(i, str) {
                if (substrRegex.test(str)) {
                    matches.push(str);
                }
            });

            cb(matches);
        };
    };

    $('.tagsinput').tagsinput({
        typeaheadjs: [
            {
                hint: true,
                highlight: true,
                minLength: 1,
            },
            {
                name: 'tags',
                source: substringMatcher($tagsInput.data('existing-tags').split(',')),
                limit: 10
            }
        ]
    });
}
