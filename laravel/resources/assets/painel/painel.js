import Clipboard from './modules/Clipboard.js';
import DataTables from './modules/DataTables.js';
import DatePicker from './modules/DatePicker.js';
import DeleteButton from './modules/DeleteButton.js';
import Filtro from './modules/Filtro.js';
import GeneratorFields from './modules/GeneratorFields.js';
import ImagesUpload from './modules/ImagesUpload.js';
import ImagesUploadProdutos from './modules/ImagesUploadProdutos.js';
import MonthPicker from './modules/MonthPicker.js';
import MultiSelect from './modules/MultiSelect.js';
import OrderImages from './modules/OrderImages.js';
import OrderTable from './modules/OrderTable.js';
import TextEditor from './modules/TextEditor.js';
import Tags from './modules/Tags.js';

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

Clipboard();
DataTables();
DatePicker();
DeleteButton();
Filtro();
GeneratorFields();
ImagesUpload();
ImagesUploadProdutos();
MonthPicker();
MultiSelect();
OrderImages();
OrderTable();
TextEditor();
Tags();

$('.handle-fornecedores').change(function() {
    const id = $(this).children('option:selected').val();
    const $linhas = $('.handle-linhas');

    $linhas
        .attr('disabled', true)
        .children('option:selected')
        .attr('selected', false);

    $.get(`${$('base').attr('href')}/painel/fornecedores/${id}/linhas`, data => {
        $linhas.children('option:not(:first-child)').remove();
        $linhas
            .append(data.map(option => `<option value="${option.id}">${option.titulo}</option>`))
            .attr('disabled', false);
    });
});

// MASK código produtos
$(".input-codigo").mask("AA00-000");
