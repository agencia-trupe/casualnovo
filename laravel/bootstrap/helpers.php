<?php

    function t($termo, $params = []) {
        return trans('frontend.'.$termo, $params);
    }

    function tobj($obj, $termo)
    {
        $lang = app()->getLocale();

        if ($lang == 'pt') return $obj->{$termo};

        return $obj->{$termo.'_'.app()->getLocale()} ?: $obj->{$termo};
    }

    function divisoes()
    {
        return [
            'interiores',
            'exteriores',
            'conceito',
            'projetos',
        ];
    }

    function prod_url($path)
    {
        return 'http://www.casualmoveis.com.br/'.str_replace('/img/', '/images/', $path);
    }
