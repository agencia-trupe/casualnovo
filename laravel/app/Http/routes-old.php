<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('marcas', 'MarcasController@index')->name('marcas');

    Route::get('produtos/tipo/{tipo_slug}/{produto_slug?}', 'ProdutosController@tipo')->name('produtos.tipo');
    Route::get('produtos/busca', 'ProdutosController@busca')->name('produtos.busca');

    Route::post('blog/comentario/{blog_post_slug}', 'BlogController@comentario')->name('blog.comentario');
	Route::get('blog/post/{blog_post_slug?}', 'BlogController@show')->name('blog.show');
	Route::get('blog/tag/{blog_tag_slug?}', 'BlogController@tag')->name('blog.tag');
	Route::get('blog/ano/{ano}/{mes?}', 'BlogController@data')->name('blog.data');
	Route::get('blog/{blog_categoria_slug?}', 'BlogController@index')->name('blog');

    Route::get('perfil', 'PerfilController@index')->name('perfil');
    Route::get('designers', 'DesignersController@index')->name('designers');
    Route::get('designers/{designer_slug}', 'DesignersController@show')->name('designers.show');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');

    Route::get('politica-de-privacidade', 'PoliticaDePrivacidadeController@index')->name('politica-de-privacidade');
    Route::post('aceite-de-cookies', 'HomeController@postCookies')->name('aceite-de-cookies.post');

    // Localização
    Route::get('lang/{idioma?}', function ($idioma = 'pt') {
        if (in_array($idioma, ['pt', 'en'])) {
            Session::put('locale', $idioma);
        }
        return redirect()->intended();
    })->name('lang');

    // Area Profissional
    Route::group([
        'prefix' => 'area-profissional',
        'namespace' => 'AreaProfissional'
    ], function() {
        Route::group(['namespace' => 'Auth'], function() {
            Route::get('login', 'AuthController@showLoginForm')
                ->name('area-profissional.login');
            Route::post('login', 'AuthController@login')
                ->name('area-profissional.auth');
            Route::get('cadastro', 'AuthController@showRegisterForm')
                ->name('area-profissional.register');
            Route::post('cadastro', 'AuthController@register')
                ->name('area-profissional.register.post');
            Route::get('logout', 'AuthController@logout')
                ->name('area-profissional.logout');
            Route::get('esqueci-minha-senha', 'PasswordController@forgotPassword')
                ->name('area-profissional.reset');
            Route::post('redefinicao-de-senha', 'PasswordController@sendResetLinkEmail')
                ->name('area-profissional.reset.email');
            Route::get('redefinicao-de-senha/{token}', 'PasswordController@showResetForm')
                ->name('area-profissional.reset.token');
            Route::post('redefinir-senha', 'PasswordController@reset')
                ->name('area-profissional.reset.post');
        });

        Route::group(['middleware' => ['auth.profissional']], function() {
            Route::get('/', function() {
                return view('frontend.area-profissional.home', [
                    'home' => \App\Models\AreaProfissional\TelaInicial::first()
                ]);
            })->name('area-profissional');

            Route::get('meu-cadastro', 'MeuCadastroController@index')->name('area-profissional.meu-cadastro');
            Route::patch('meu-cadastro', 'MeuCadastroController@update')->name('area-profissional.meu-cadastro.update');

            Route::get('orcamento', 'OrcamentoController@index')
                ->name('area-profissional.orcamento');
            Route::post('orcamento', 'OrcamentoController@post')
                ->name('area-profissional.orcamento.post');
            Route::get('orcamento/historico/{id?}', 'OrcamentoController@history')
                ->name('area-profissional.orcamento.historico');
            Route::post('orcamento/{id}', 'OrcamentoController@add')
                ->name('area-profissional.orcamento.adicionar');
            Route::get('orcamento/{id}/remover', 'OrcamentoController@remove')
                ->name('area-profissional.orcamento.remover');

            Route::get('relacionamento', 'RelacionamentoController@index')
                ->name('area-profissional.relacionamento');
            Route::get('relacionamento/regulamento', 'RelacionamentoController@regulamento')
                ->name('area-profissional.relacionamento.regulamento');
            Route::get('relacionamento/extrato', 'RelacionamentoController@extrato')
                ->name('area-profissional.relacionamento.extrato');
        });
    });

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('enderecos', 'EnderecosController');
		Route::get('produtos', 'ProdutosController@all')->name('painel.produtos.all');
        Route::post('produtos/imagens/upload', 'ProdutosImagensController@upload')
            ->name('painel.produtos.imagens.upload');
		Route::resource('fornecedores', 'FornecedoresController');
		Route::resource('fornecedores.linhas', 'LinhasController');
		Route::resource('fornecedores.linhas.produtos', 'ProdutosController');
		Route::get('fornecedores/{fornecedores}/linhas/{linhas}/produtos/{produtos}/imagens/clear', [
			'as'   => 'painel.fornecedores.linhas.produtos.imagens.clear',
			'uses' => 'ProdutosImagensController@clear'
		]);
		Route::resource('fornecedores.linhas.produtos.imagens', 'ProdutosImagensController', ['parameters' => ['imagens' => 'imagens_produtos']]);
		Route::resource('produtos-materiais', 'ProdutosMateriaisController');
        Route::resource('tipos', 'TiposController');

		Route::resource('chamadas', 'ChamadasController');
		Route::resource('home', 'HomeController', ['only' => ['index', 'update']]);
		Route::resource('banners', 'BannersController');
		Route::resource('marcas', 'MarcasController');
		Route::resource('perfil', 'PerfilController', ['only' => ['index', 'update']]);
		Route::resource('designers-frase', 'DesignersFraseController', ['only' => ['index', 'update']]);
		Route::resource('designers', 'DesignersController');
		Route::get('designers/{designers}/imagens/clear', [
			'as'   => 'painel.designers.imagens.clear',
			'uses' => 'DesignersImagensController@clear'
		]);
        Route::resource('designers.imagens', 'DesignersImagensController', ['parameters' => ['imagens' => 'imagens_designers']]);

        Route::resource('blog/categorias', 'BlogCategoriasController', ['parameters' => ['categorias' => 'categorias_blog']]);
        Route::resource('blog', 'BlogPostsController', ['parameters' => ['blog' => 'posts']]);
        Route::resource('blog.comentarios', 'BlogComentariosController', ['parameters' => ['blog' => 'posts'], ['only' => ['index', 'show', 'update']]]);

        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');

        Route::resource('politica-de-privacidade', 'PoliticaDePrivacidadeController', ['only' => ['index', 'update']]);
        Route::get('aceite-de-cookies', 'AceiteDeCookiesController@index')->name('painel.aceite-de-cookies');

        // Painel / Area Profissional
        Route::group([
            'prefix'    => 'area-profissional',
            'namespace' => 'AreaProfissional',
        ], function() {
            Route::resource('e-mail-avisos', 'EMailAvisosController', ['only' => ['index', 'update']]);
            Route::resource('imagem-de-capa', 'ImagemDeCapaController', ['only' => ['index', 'update']]);
            Route::get('usuarios/newsletter', 'UsuariosController@newsletter')->name('painel.area-profissional.usuarios.newsletter');
            Route::resource('tela-inicial', 'TelaInicialController', ['only' => ['index', 'update']]);
            Route::resource('usuarios', 'UsuariosController', ['only' => ['index', 'show', 'update', 'destroy'], 'parameters' => ['usuarios' => 'usuarios-catalogo']]);
            Route::resource('orcamentos', 'OrcamentosController', ['only' => ['index', 'show', 'destroy']]);
        });

        Route::resource('usuarios', 'UsuariosController');
		Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Painel / Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
