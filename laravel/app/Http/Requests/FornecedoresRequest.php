<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class FornecedoresRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'divisao' => 'required|array',
            'titulo' => 'required',
            'imagem' => 'required|image',
            'obs' => '',
            'publicar_site' => '',
            'publicar_catalogo' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
