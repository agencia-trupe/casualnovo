<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class IntHomeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'imagem'   => 'image',
            'imagem_2' => 'image',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem']   = 'image';
            $rules['imagem_2'] = 'image';
        }

        return $rules;
    }
}
