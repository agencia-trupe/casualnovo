<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DesignersFraseRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'frase' => 'required',
            'frase_en' => 'required',
            'frase_es' => 'required',
        ];
    }
}
