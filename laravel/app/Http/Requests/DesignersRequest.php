<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DesignersRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome' => 'required',
            'foto' => 'required|image',
            'texto' => 'required',
            'texto_en' => 'required',
            'texto_es' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['foto'] = 'image';
        }

        return $rules;
    }
}
