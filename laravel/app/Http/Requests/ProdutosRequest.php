<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'divisao' => 'required|array',
            'tipos_id' => 'required',
            'origem' => 'required',
            'titulo' => 'required',
            'titulo_en' => '',
            'titulo_es' => '',
            'item' => '',
            'item_en' => '',
            'item_es' => '',
            'codigo' => 'required|max:8',
            'imagem_capa' => 'required|image',
            'descritivo' => '',
            'descritivo_en' => '',
            'descritivo_es' => '',
            'designer' => '',
            'dimensoes' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['fornecedores_id'] = 'required';
            $rules['linhas_id']       = 'required';
            $rules['imagem_capa']     = 'image';
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'tipos_id' => 'tipo',
        ];
    }
}
