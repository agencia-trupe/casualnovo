<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EnderecosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'titulo_en' => 'required',
            'google_maps' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
