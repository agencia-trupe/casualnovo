<?php

namespace App\Http\Requests\AreaProfissional;

use App\Http\Requests\Request;

class TelaInicialRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'texto' => 'required',
            'texto_en' => '',
            'imagem' => 'image',
            'imagem_en' => 'image',
        ];
    }
}
