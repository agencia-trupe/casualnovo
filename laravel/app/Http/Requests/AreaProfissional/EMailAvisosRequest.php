<?php

namespace App\Http\Requests\AreaProfissional;

use App\Http\Requests\Request;

class EMailAvisosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'required|email',
        ];
    }
}
