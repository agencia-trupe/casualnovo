<?php

namespace App\Http\Requests\AreaProfissional;

use App\Http\Requests\Request;

class ImagemDeCapaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'imagem' => 'required|image',
        ];
    }
}
