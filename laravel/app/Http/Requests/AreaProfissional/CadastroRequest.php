<?php

namespace App\Http\Requests\AreaProfissional;

use App\Helpers\Tools;
use App\Http\Requests\Request;

class CadastroRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome'        => 'required',
            'email'       => 'required|email|unique:usuarios_catalogo',
            'cpf'         => 'required_if:pais,Brasil',
            'telefone'    => 'required',
            'empresa'     => 'required',
            'cargo'       => 'required',
            'pais'        => 'required|in:'.implode(',', array_keys(Tools::listaPaises())),
            'cep'         => 'required',
            'endereco'    => 'required',
            'numero'      => 'required',
            'complemento' => '',
            'cidade'      => 'required',
            'estado'      => 'required_if:pais,Brasil|max:2',
            'password'    => 'required|confirmed|min:6',
        ];

        if ($this->method() != 'POST') {
            $id = auth('profissional')->user()->id;

            $rules['email'] = 'required|email|unique:usuarios_catalogo,email,'.$id;
            $rules['password'] = 'confirmed|min:6';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'cpf.required_if'    => trans('validation.required', ['attribute' => 'CPF']),
            'estado.required_if' => trans('validation.required', ['attribute' => t('profissional.estado')])
        ];
    }
}
