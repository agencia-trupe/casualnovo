<?php

namespace App\Http\Requests\AreaProfissional;

use App\Http\Requests\Request;

class OrcamentoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
       $rules = [];

       foreach($this->request->get('quantidade') as $key => $val)
        {
            $rules['quantidade.'.$key] = 'required|integer|min:1';
        }

        return $rules;
    }

    public function messages()
    {
        $messages = [];

        foreach ($this->request->get('quantidade') as $key => $val) {
            $messages['quantidade.'.$key.'.*'] = t('profissional.quantidade', ['num' => $key + 1]);
        }

        return $messages;
    }
}
