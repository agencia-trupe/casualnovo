<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BlogPostsRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'blog_categorias_id' => 'required',
            'titulo'             => 'required',
            'titulo_en'          => '',
            'titulo_es'          => '',
            'data'               => 'required',
            'capa'               => 'required|image',
            'texto'              => 'required',
            'texto_en'           => '',
            'texto_es'           => '',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'blog_categorias_id' => 'categoria'
        ];
    }
}
