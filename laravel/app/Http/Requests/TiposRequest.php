<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TiposRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'titulo_en' => '',
            'titulo_es' => '',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
