<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class HomeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'imagem'   => 'required|image',
            'imagem_2' => 'required|image',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem']   = 'image';
            $rules['imagem_2'] = 'image';
        }

        return $rules;
    }
}
