<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutosMateriaisRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'titulo_en' => '',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
