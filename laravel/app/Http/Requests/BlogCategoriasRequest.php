<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BlogCategoriasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'titulo' => 'required',
            'titulo_en' => 'required',
            'titulo_es' => 'required',
        ];
    }
}
