<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MarcasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'divisao' => 'required|array',
            'nome' => 'required',
            'imagem' => 'required|image',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
