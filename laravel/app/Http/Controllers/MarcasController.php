<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Marca;

class MarcasController extends Controller
{
    public function index(Request $request)
    {
        if ($divisao = $request->divisao) {
            $marcas = Marca::where('divisao', 'LIKE', "%$divisao%")
                ->brasil()->ordenados()->get();
        } else {
            $marcas = Marca::brasil()->ordenados()->get();
        }

        return view('frontend.marcas', compact('marcas'));
    }
}
