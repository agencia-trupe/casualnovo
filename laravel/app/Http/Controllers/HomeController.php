<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AceiteDeCookies;
use App\Models\Banner;
use App\Models\Chamada;
use App\Models\Home;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $banners  = Banner::ordenados()->get();
        $home     = Home::first();
        $chamadas = Chamada::brasil()->ordenados()->get();

        $verificacao = AceiteDeCookies::where('ip', $request->ip())->first();

        return view('frontend.home', compact('banners', 'home', 'chamadas', 'verificacao'));
    }

    public function postCookies(Request $request)
    {
        $input = $request->all();
        $input['ip'] = $request->ip();

        AceiteDeCookies::create($input);

        return redirect('/');
    }
}
