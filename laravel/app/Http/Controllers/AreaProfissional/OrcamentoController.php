<?php

namespace App\Http\Controllers\AreaProfissional;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use App\Http\Requests\AreaProfissional\OrcamentoRequest;
use App\Models\AreaProfissional\EMailAvisos;
use App\Models\AreaProfissional\Orcamento;
use App\Models\Produto;

class OrcamentoController extends Controller
{
    private $orcamento;

    public function __construct()
    {
        if (! auth('profissional')->user()->podeSolicitarOrcamento) {
            return redirect()->route('home')->send();
        }

        $this->orcamento = session()->get('orcamento', []);
    }

    public function index()
    {
        $itens = $this->getItens();

        return view('frontend.area-profissional.orcamento.index', compact('itens'));
    }

    public function post(OrcamentoRequest $request)
    {
        try {

            $itens = $this->getItens();

            $quantidades = $request->get('quantidade');
            $observacoes = $request->get('observacoes');

            $orcamento = Orcamento::create([
                'usuario_id'  => auth('profissional')->user()->id,
                'observacoes' => $observacoes,
                'orcamento'   => json_encode($itens->map(
                    function($item, $index) use ($quantidades, $observacoes) {
                        return [
                            'id'         => $item->id,
                            'codigo'     => $item->codigo,
                            'divisao'    => $item->divisao,
                            'imagem'     => $item->imagem_capa,
                            'quantidade' => (int) $quantidades[$index],
                        ];
                    })
                ),
            ]);

            session()->put('orcamento', []);

            try {
                $email = EMailAvisos::firstOrFail()->email;

                if ($email) {
                    Mail::send('emails.area-profissional.orcamento', compact('orcamento'), function($m) use ($email) {
                        $m->to($email, config('app.name'))
                        ->subject('[ORÇAMENTO] '.config('app.name'));
                    });
                }
            } catch (\Exception $e) {}

            return redirect()->route('area-profissional.orcamento')->with('enviado', true);

        } catch (\Exception $e) {

            return back()->withErrors([t('profissional.orcamento-erro')]);

        }
    }

    public function history($id = null)
    {
        $user = auth('profissional')->user();

        if ($id) {
            $orcamentoHistorico = Orcamento::where([
                'id'         => $id,
                'usuario_id' => $user->id,
            ])->firstOrFail();

            return view('frontend.area-profissional.orcamento.show', compact('orcamentoHistorico'));
        }

        $orcamentos = Orcamento::where('usuario_id', $user->id)->orderBy('created_at', 'DESC')->get();

        return view('frontend.area-profissional.orcamento.historico', compact('orcamentos'));
    }

    public function add($id)
    {
        $produto = Produto::find($id);

        if (! $produto) {
            return response()->json('Produto não encontrado.', 500);
        }

        if (! in_array($produto->id, $this->orcamento)) {
            $this->orcamento[] = $produto->id;
            session()->put('orcamento', $this->orcamento);
        }

        return $this->orcamento;
    }

    public function remove($id)
    {
        if (in_array($id, $this->orcamento)) {
            session()->put('orcamento', array_diff($this->orcamento, [$id]));
        }

        return redirect()->route('area-profissional.orcamento');
    }

    protected function getItens()
    {
        $produtos = Produto::with('tipo')
            ->whereIn('id', $this->orcamento)
            ->get();

        return collect($this->orcamento)
            ->map(function($id) use ($produtos) {
                return $produtos->first(function($index, $produto) use ($id) {
                    return $produto->id == $id;
                });
            })->filter(function($item) {
                return !!$item;
            });
    }
}
