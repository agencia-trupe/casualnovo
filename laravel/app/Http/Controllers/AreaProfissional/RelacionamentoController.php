<?php

namespace App\Http\Controllers\AreaProfissional;

use App\Http\Controllers\Controller;
use Carbon\Carbon;

use App\Models\AreaProfissional\ProgramaCampanhas;
use App\Models\AreaProfissional\ProgramaPremios;

class RelacionamentoController extends Controller
{
    private $user;
    private $campanha;

    public function __construct()
    {
        if (! auth('profissional')->user()->participaProgramaRelacionamento) {
            return redirect()->route('home')->send();
        }

        $this->user     = auth('profissional')->user();
        $this->campanha = ProgramaCampanhas::ativa()->first();

        $user       = $this->user;
        $campanha   = $this->campanha;
        $saldoTotal = $user->pontuacaoTotal(null, false);

        $diasFaltantes = Carbon::createFromFormat('Y-m-d', $campanha->data_termino)
            ->diffInDays(Carbon::now());

        $premio = $campanha
            ->premios($user->tipo_participacao_relacionamento)
            ->pontos($saldoTotal)
            ->first();

		if ($premio) {
			$proximoPremio = ProgramaPremios::where('inicio_pontuacao', '>', $premio->getOriginal('inicio_pontuacao'))
                ->where('programa_campanha_id', '=', $premio->programa_campanha_id)
                ->where('tipo', '=', $user->tipo_participacao_relacionamento)
                ->orderBy('inicio_pontuacao', 'asc')
                ->first();
		} else {
			$proximoPremio = ProgramaPremios::where('inicio_pontuacao', '>', $saldoTotal * 100)
                ->where('programa_campanha_id', '=', $campanha->id)
                ->where('tipo', '=', $user->tipo_participacao_relacionamento)
                ->orderBy('inicio_pontuacao', 'asc')
                ->first();
        }

        view()->share(compact(
            'campanha',
            'saldoTotal',
            'diasFaltantes',
            'premio',
            'proximoPremio'
        ));
    }

    public function index()
    {
        return view('frontend.area-profissional.relacionamento.index');
    }

    public function extrato()
    {
        return view('frontend.area-profissional.relacionamento.extrato');
    }

    public function regulamento()
    {
        $premios = $this->campanha
            ->premios($this->user->tipo_participacao_relacionamento)
            ->get();

        return view('frontend.area-profissional.relacionamento.regulamento', compact('premios'));
    }
}
