<?php

namespace App\Http\Controllers\AreaProfissional\Auth;

use Auth;
use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Models\AreaProfissional\Usuario;
use App\Http\Requests\AreaProfissional\CadastroRequest;
use App\Models\AreaProfissional\EMailAvisos;

class AuthController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectTo = '/area-profissional';
    protected $guard      = 'profissional';

    public function __construct()
    {
        view()->share('capa', \App\Models\AreaProfissional\ImagemDeCapa::first());

        $this->middleware('guest.profissional', ['except' => 'logout']);
    }

    public function showRegisterForm()
    {
        return view('frontend.area-profissional.auth.register');
    }

    public function register(CadastroRequest $request)
    {
        $input = $request->only([
            'nome',
            'email',
            'cpf',
            'telefone',
            'empresa',
            'cargo',
            'pais',
            'cep',
            'endereco',
            'numero',
            'complemento',
            'cidade',
            'estado',
            'password',
            'receber_newsletter'
        ]);

        $input['password'] = bcrypt($input['password']);

        $user = Usuario::create($input);

        try {
            $email = EMailAvisos::firstOrFail()->email;

            if ($email) {
                Mail::send('emails.area-profissional.cadastro', $user->toArray(), function($m) use ($email) {
                    $m->to($email, config('app.name'))
                    ->subject('[NOVO CADASTRO] '.config('app.name'));
                });
            }
        } catch (\Exception $e) {}

        auth('profissional')->login($user);

        return redirect()->route('area-profissional');
    }

    public function showLoginForm()
    {
        return view('frontend.area-profissional.auth.login');
    }

    protected function login(Request $request)
    {
        if (Auth::guard('profissional')->attempt([
            'email'    => $request->input('email'),
            'password' => $request->input('password')
        ], true)) {
            return redirect()->intended('area-profissional');
        } else {
            return redirect()->route('area-profissional.login')
                ->withInput()->with('error', t('profissional.login-invalido'));
        }
    }
}
