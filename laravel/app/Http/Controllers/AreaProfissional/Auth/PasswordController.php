<?php

namespace App\Http\Controllers\AreaProfissional\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Password;

use Illuminate\Http\Request;

class PasswordController extends Controller
{
    use ResetsPasswords;

    protected $subject = 'Casual Móveis · Redefinição de senha';

    protected $guard  = 'profissional';
    protected $broker = 'profissionais';

    public function __construct()
    {
        view()->share('capa', \App\Models\AreaProfissional\ImagemDeCapa::first());

        $this->middleware('guest.profissional');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email'
        ]);

        $broker = $this->getBroker();

        $response = Password::broker($broker)->sendResetLink($request->only('email'), function (\Illuminate\Mail\Message $message) {
            $message->subject($this->getEmailSubject());
        });

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return redirect()->back()
                    ->with('sent', t('profissional.redefinicao-email'));
            case Password::INVALID_USER:
            default:
                return redirect()->back()
                    ->with('error', t('profissional.erro-email'));
        }
    }

    public function forgotPassword()
    {
        return view('frontend.area-profissional.auth.forgot-password');
    }

    public function showResetForm(Request $request, $token = null)
    {
        $email = $request->input('email');

        return view('frontend.area-profissional.auth.reset', compact('email', 'token'));
    }

    public function reset(Request $request)
    {
        $this->validate($request, $this->getResetValidationRules());

        $credentials = $request->only(
            'email',
            'password',
            'password_confirmation',
            'token'
        );

        $broker = $this->getBroker();

        $response = Password::broker($broker)->reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });

        switch ($response) {
            case Password::PASSWORD_RESET:
                return $this->getResetSuccessResponse($response);

            default:
                return $this->getResetFailureResponse($request, $response);
        }
    }

    protected function resetPassword($user, $password)
    {
        $user->password = bcrypt($password);
        $user->save();
    }

    protected function getResetSuccessResponse()
    {
        return redirect()->route('area-profissional.login')
            ->with('reset', t('profissional.senha-redefinida'));
    }
}
