<?php

namespace App\Http\Controllers\AreaProfissional;

use App\Http\Controllers\Controller;
use App\Http\Requests\AreaProfissional\CadastroRequest;

class MeuCadastroController extends Controller
{
    public function index()
    {
        $user = auth('profissional')->user();

        return view('frontend.area-profissional.meu-cadastro', compact('user'));
    }

    public function update(CadastroRequest $request)
    {
        try {

            $user = auth('profissional')->user();

            $input = $request->only([
                'nome',
                'email',
                'cpf',
                'telefone',
                'empresa',
                'cargo',
                'pais',
                'cep',
                'endereco',
                'numero',
                'complemento',
                'cidade',
                'estado',
                'password',
                'receber_newsletter'
            ]);

            if (isset($input['password']) && strlen($input['password'])) {
                $input['password'] = bcrypt($input['password']);
            } else {
                unset($input['password']);
            }

            $user->update($input);

            return back()->with('success', t('profissional.cadastro-alterado'));

        } catch (\Exception $e) {

            return back()->withErrors([t('profissional.cadastro-erro')]);

        }
    }
}
