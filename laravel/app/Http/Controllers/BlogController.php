<?php

namespace App\Http\Controllers;

use App\Http\Requests\BlogComentariosRequest;

use App\Models\BlogCategoria;
use App\Models\BlogPost;
use App\Models\BlogTag;

class BlogController extends Controller
{
    private $postsPerPage = 5;

    private $categorias;
    private $tags;
    private $anos;

    public function __construct()
    {
        $this->categorias = BlogCategoria::whereHas('posts', function($query) {
            $query->publicados()->brasil();
        })->get();

        $this->tags = BlogTag::whereHas('posts', function($query) {
            $query->publicados()->brasil();
        })->ordenados()->get();

        $this->anos = BlogPost::publicados()->brasil()
            ->distinct()
            ->select(\DB::raw('YEAR(data) as ano'))
            ->groupBy('ano')
            ->orderBy('ano', 'DESC')
            ->get();
    }

    public function index(BlogCategoria $categoria)
    {
        $posts = $categoria->exists
            ? $categoria->posts()
            : BlogPost::query();

        $posts = $posts
            ->with('categoria', 'comentariosAprovados', 'tags')
            ->publicados()
            ->brasil()
            ->ordenados()
            ->paginate($this->postsPerPage);

        return view('frontend.blog.index', [
            'categorias' => $this->categorias,
            'tags'       => $this->tags,
            'anos'       => $this->anos,
            'categoria'  => $categoria,
            'posts'      => $posts,
        ]);
    }

    public function data($ano, $mes = null)
    {
        $meses = BlogPost::publicados()->brasil()
            ->distinct()
            ->select(\DB::raw('MONTH(data) as mes'))
            ->groupBy('mes')
            ->orderBy('mes', 'DESC')
            ->whereRaw("YEAR(data) = $ano")
            ->get();

        $posts = BlogPost::with('categoria', 'comentariosAprovados', 'tags')
            ->publicados()
            ->brasil()
            ->ordenados();

        if ($mes) {
            $posts = $posts->whereRaw("YEAR(data) = $ano AND MONTH(data) = $mes");
        } else {
            $posts = $posts->whereRaw("YEAR(data) = $ano");
        }

        $posts = $posts->paginate($this->postsPerPage);

        return view('frontend.blog.index', [
            'categorias' => $this->categorias,
            'tags'       => $this->tags,
            'anos'       => $this->anos,
            'meses'      => $meses,
            'posts'      => $posts,
            'ano'        => $ano,
            'mes'        => $mes
        ]);
    }

    public function tag(BlogTag $tag)
    {
        $posts = BlogPost::with('categoria', 'comentariosAprovados', 'tags')
            ->publicados()
            ->brasil()
            ->ordenados()
            ->whereHas('tags', function($query) use ($tag) {
                $query->whereSlug($tag->slug);
            })
            ->paginate($this->postsPerPage);

        return view('frontend.blog.index', [
            'categorias' => $this->categorias,
            'tags'       => $this->tags,
            'anos'       => $this->anos,
            'tag'        => $tag,
            'posts'      => $posts,
        ]);
    }

    public function show(BlogPost $post)
    {
        return view('frontend.blog.show', [
            'categorias' => $this->categorias,
            'tags'       => $this->tags,
            'anos'       => $this->anos,
            'post'       => $post,
            'categoria'  => $post->categoria,
        ]);
    }

    public function comentario(BlogComentariosRequest $request, BlogPost $post)
    {
        $post->comentarios()->create([
            'autor'    => request('autor'),
            'email'    => request('email'),
            'texto'    => request('comentario'),
            'data'     => Date('Y-m-d'),
            'aprovado' => false
        ]);

        return back()->with('comentarioEnviado', true);
    }
}
