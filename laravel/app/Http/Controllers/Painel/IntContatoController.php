<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\IntContatoRequest;
use App\Models\IntContato;

class IntContatoController extends Controller
{
    public function index()
    {
        $contato = IntContato::first();

        return view('painel.internacional.contato.index', compact('contato'));
    }

    public function update(IntContatoRequest $request, $contato)
    {
        try {
            $input = $request->all();

            $contato = IntContato::first();
            $contato->update($input);

            return redirect()->route('painel.internacional.contato.index')->with('success', 'Informações alteradas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar informações: ' . $e->getMessage()]);
        }
    }
}
