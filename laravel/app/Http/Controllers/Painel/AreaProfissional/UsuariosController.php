<?php

namespace App\Http\Controllers\Painel\AreaProfissional;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

use App\Models\AreaProfissional\Usuario;

class UsuariosController extends Controller
{
    public function index(Request $request)
    {
        if ($busca = $request->get('busca')) {
            $usuarios = Usuario::orderBy('nome', 'ASC')
                ->where('nome', 'LIKE', "%$busca%")
                ->orWhere('email', 'LIKE', "%$busca%")
                ->paginate(20);
        } else {
            $usuarios = Usuario::orderBy('nome', 'ASC')->paginate(20);
        }

        return view('painel.area-profissional.usuarios.index', compact('usuarios'));
    }

    public function show(Usuario $usuario)
    {
        return view('painel.area-profissional.usuarios.show', compact('usuario'));
    }

    public function update(Request $request, Usuario $usuario)
    {
        try {

            $input = $request->only(['is_funcionario', 'bloqueado']);

            $usuario->update($input);

            return redirect()->route('painel.area-profissional.usuarios.show', $usuario->id)->with('success', 'Usuário alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar usuário: '.$e->getMessage()]);

        }
    }

    public function destroy(Usuario $usuario)
    {
        try {

            $usuario->delete();

            return back()->with('success', 'Usuário excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir usuário: '.$e->getMessage()]);

        }
    }

    public function newsletter()
    {
        $registros = Usuario::where('receber_newsletter', 1)
            ->orderBy('nome', 'ASC')
            ->get([
                'nome',
                'email as e-mail',
            ]);

        $fileName = 'CasualMoveis-Newsletter_'.date('d-m-Y_His');

        Excel::create($fileName, function ($excel) use ($registros) {
            $excel->sheet('newsletter', function ($sheet) use ($registros) {
                $sheet->fromModel($registros);
            });
        })->download('csv');
    }
}
