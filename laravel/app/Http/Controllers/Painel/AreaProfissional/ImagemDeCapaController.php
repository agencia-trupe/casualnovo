<?php

namespace App\Http\Controllers\Painel\AreaProfissional;

use Illuminate\Http\Request;

use App\Http\Requests\AreaProfissional\ImagemDeCapaRequest;
use App\Http\Controllers\Controller;

use App\Models\AreaProfissional\ImagemDeCapa;

class ImagemDeCapaController extends Controller
{
    public function index()
    {
        $registro = ImagemDeCapa::first();

        return view('painel.area-profissional.imagem-de-capa.edit', compact('registro'));
    }

    public function update(ImagemDeCapaRequest $request, ImagemDeCapa $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = ImagemDeCapa::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.area-profissional.imagem-de-capa.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
