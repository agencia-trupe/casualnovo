<?php

namespace App\Http\Controllers\Painel\AreaProfissional;

use Illuminate\Http\Request;

use App\Http\Requests\AreaProfissional\EMailAvisosRequest;
use App\Http\Controllers\Controller;

use App\Models\AreaProfissional\EMailAvisos;

class EMailAvisosController extends Controller
{
    public function index()
    {
        $registro = EMailAvisos::first();

        return view('painel.area-profissional.e-mail-avisos.edit', compact('registro'));
    }

    public function update(EMailAvisosRequest $request, EMailAvisos $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.area-profissional.e-mail-avisos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
