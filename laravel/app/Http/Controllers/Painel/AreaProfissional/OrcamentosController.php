<?php

namespace App\Http\Controllers\Painel\AreaProfissional;

use App\Http\Controllers\Controller;

use App\Models\AreaProfissional\Orcamento;

class OrcamentosController extends Controller
{
    public function index()
    {
        $orcamentos = Orcamento::with('usuario')
            ->orderBy('created_at', 'DESC')
            ->paginate(20);

        return view('painel.area-profissional.orcamentos.index', compact('orcamentos'));
    }

    public function show(Orcamento $orcamento)
    {
        return view('painel.area-profissional.orcamentos.show', compact('orcamento'));
    }

    public function destroy(Orcamento $orcamento)
    {
        try {

            $orcamento->delete();

            return back()->with('success', 'Orçamento excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir orçamento: '.$e->getMessage()]);

        }
    }
}
