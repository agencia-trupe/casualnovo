<?php

namespace App\Http\Controllers\Painel\AreaProfissional;

use Illuminate\Http\Request;

use App\Http\Requests\AreaProfissional\TelaInicialRequest;
use App\Http\Controllers\Controller;

use App\Models\AreaProfissional\TelaInicial;

class TelaInicialController extends Controller
{
    public function index()
    {
        $registro = TelaInicial::first();

        return view('painel.area-profissional.tela-inicial.edit', compact('registro'));
    }

    public function update(TelaInicialRequest $request, TelaInicial $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = TelaInicial::upload_imagem();
            if (isset($input['imagem_en'])) $input['imagem_en'] = TelaInicial::upload_imagem_en();

            $registro->update($input);

            return redirect()->route('painel.area-profissional.tela-inicial.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
