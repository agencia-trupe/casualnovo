<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\FornecedoresRequest;
use App\Http\Controllers\Controller;

use App\Models\Fornecedor;

class FornecedoresController extends Controller
{
    public function index()
    {
        if (isset($_GET['local'])) {
            $local = $_GET['local'];
            if ($local == 'brasil') {
                $registros = Fornecedor::brasil()->ordenados()->get();
            }
            if ($local == 'internacional') {
                $registros = Fornecedor::internacional()->ordenados()->get();
            }
        } else {
            $registros = Fornecedor::ordenados()->get();
        }

        return view('painel.fornecedores.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.fornecedores.create');
    }

    public function store(FornecedoresRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Fornecedor::upload_imagem();

            $input['divisao'] = implode(',', $request->get('divisao'));

            Fornecedor::create($input);

            return redirect()->route('painel.fornecedores.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Fornecedor $registro)
    {
        return view('painel.fornecedores.edit', compact('registro'));
    }

    public function update(FornecedoresRequest $request, Fornecedor $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Fornecedor::upload_imagem();

            $input['divisao'] = implode(',', $request->get('divisao'));

            $registro->update($input);

            return redirect()->route('painel.fornecedores.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Fornecedor $registro)
    {
        try {

            if (count($registro->linhas)) {
                throw new \Error('Não é possível excluir fornecedores que possuem linhas.');
            }

            $registro->delete();

            return redirect()->route('painel.fornecedores.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
