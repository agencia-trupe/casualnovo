<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\IntHomeRequest;
use App\Models\IntHome;

class IntHomeController extends Controller
{
    public function index()
    {
        $registro = IntHome::first();

        return view('painel.internacional.home.edit', compact('registro'));
    }

    public function update(IntHomeRequest $request, $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = IntHome::upload_imagem();
            if (isset($input['imagem_2'])) $input['imagem_2'] = IntHome::upload_imagem_2();

            $registro = IntHome::where('id', $registro->id)->first();
            $registro->update($input);

            return redirect()->route('painel.internacional.home.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
