<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\DesignersRequest;
use App\Http\Controllers\Controller;

use App\Models\Designer;

class DesignersController extends Controller
{
    public function index()
    {
        if (isset($_GET['local'])) {
            $local = $_GET['local'];
            if ($local == 'brasil') {
                $registros = Designer::brasil()->ordenados()->get();
            }
            if ($local == 'internacional') {
                $registros = Designer::internacional()->ordenados()->get();
            }
        } else {
            $registros = Designer::ordenados()->get();
        }

        return view('painel.designers.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.designers.create');
    }

    public function store(DesignersRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['foto'])) $input['foto'] = Designer::upload_foto();

            Designer::create($input);

            return redirect()->route('painel.designers.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Designer $registro)
    {
        return view('painel.designers.edit', compact('registro'));
    }

    public function update(DesignersRequest $request, Designer $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['foto'])) $input['foto'] = Designer::upload_foto();

            $registro->update($input);

            return redirect()->route('painel.designers.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Designer $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.designers.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
