<?php

namespace App\Http\Controllers\Painel;

use App\Http\Requests\BlogCategoriasRequest;
use App\Http\Controllers\Controller;

use App\Models\BlogCategoria;

class BlogCategoriasController extends Controller
{
    public function index()
    {
        $registros = BlogCategoria::ordenados()->get();

        return view('painel.blog.categorias.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.blog.categorias.create');
    }

    public function store(BlogCategoriasRequest $request)
    {
        try {

            $input = $request->all();

            BlogCategoria::create($input);

            return redirect()->route('painel.blog.categorias.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(BlogCategoria $registro)
    {
        return view('painel.blog.categorias.edit', compact('registro'));
    }

    public function update(BlogCategoriasRequest $request, BlogCategoria $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.blog.categorias.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(BlogCategoria $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.blog.categorias.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
