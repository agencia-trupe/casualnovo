<?php

namespace App\Http\Controllers\Painel;

use App\Http\Requests\LinhasRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Fornecedor;
use App\Models\Linha;

class LinhasController extends Controller
{
    public function index(Fornecedor $fornecedor, Request $request)
    {
        $registros = $fornecedor->linhas;

        if ($request->ajax()) {
            return $registros;
        }

        return view('painel.linhas.index', compact('fornecedor', 'registros'));
    }

    public function create(Fornecedor $fornecedor)
    {
        return view('painel.linhas.create', compact('fornecedor'));
    }

    public function store(LinhasRequest $request, Fornecedor $fornecedor)
    {
        try {

            $input = $request->all();

            $fornecedor->linhas()->create($input);

            return redirect()->route('painel.fornecedores.linhas.index', $fornecedor->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Fornecedor $fornecedor, Linha $registro)
    {
        return view('painel.linhas.edit', compact('fornecedor', 'registro'));
    }

    public function update(LinhasRequest $request, Fornecedor $fornecedor, Linha $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.fornecedores.linhas.index', $fornecedor->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Fornecedor $fornecedor, Linha $registro)
    {
        try {

            if (count($registro->produtos)) {
                throw new \Error('Não é possível excluir linhas que possuem produtos.');
            }

            $registro->delete();

            return redirect()->route('painel.fornecedores.linhas.index', $fornecedor->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
