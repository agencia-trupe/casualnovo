<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\IntConfiguracoesRequest;
use App\Models\IntConfiguracoes;

class IntConfiguracoesController extends Controller
{
    public function index()
    {
        $registro = IntConfiguracoes::first();

        return view('painel.internacional.configuracoes.edit', compact('registro'));
    }

    public function update(IntConfiguracoesRequest $request, $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_de_compartilhamento'])) $input['imagem_de_compartilhamento'] = IntConfiguracoes::upload_imagem_de_compartilhamento();

            $registro = IntConfiguracoes::first();
            $registro->update($input);

            return redirect()->route('painel.internacional.configuracoes.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
