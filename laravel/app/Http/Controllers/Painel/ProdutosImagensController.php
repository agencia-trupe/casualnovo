<?php

namespace App\Http\Controllers\Painel;

use App\Http\Requests\ProdutosImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Fornecedor;
use App\Models\Linha;
use App\Models\Produto;
use App\Models\ProdutoImagem;

class ProdutosImagensController extends Controller
{
    public function index(Fornecedor $fornecedor, Linha $linha, Produto $registro)
    {
        $imagens = ProdutoImagem::produto($registro->id)->ordenados()->get();

        return view('painel.produtos.imagens.index', compact('fornecedor', 'linha', 'imagens', 'registro'));
    }

    public function show(Fornecedor $fornecedor, Linha $linha, Produto $registro, ProdutoImagem $imagem)
    {
        return $imagem;
    }

    public function store(Fornecedor $fornecedor, Linha $linha, Produto $registro, ProdutosImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = ProdutoImagem::uploadImagem();

            $imagem = $registro->imagens()->create($input);

            $view = view('painel.produtos.imagens.imagem', compact('registro', 'imagem', 'fornecedor', 'linha'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Fornecedor $fornecedor, Linha $linha, Produto $registro, ProdutoImagem $imagem)
    {
        try {

            $imagem->delete();

            return redirect()->route('painel.fornecedores.linhas.produtos.imagens.index', [$fornecedor, $linha, $registro])->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Fornecedor $fornecedor, Linha $linha, Produto $registro)
    {
        try {

            $registro->imagens()->delete();
            return redirect()->route('painel.fornecedores.linhas.produtos.imagens.index', [$fornecedor, $linha, $registro])->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }

    public function upload(ProdutosImagensRequest $request)
    {
        try {

            $input = $request->all();

            $imagem = ProdutoImagem::uploadImagem();

            $view = view('painel.produtos.imagem', compact('imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }
}
