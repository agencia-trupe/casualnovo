<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\IntBannersRequest;
use App\Models\IntBanner;

class IntBannersController extends Controller
{
    public function index()
    {
        $registros = IntBanner::ordenados()->get();

        return view('painel.internacional.banners.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.internacional.banners.create');
    }

    public function store(IntBannersRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = IntBanner::upload_imagem();

            IntBanner::create($input);

            return redirect()->route('painel.internacional.banners.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit($id)
    {
        $registro = IntBanner::where('id', $id)->first();

        return view('painel.internacional.banners.edit', compact('registro'));
    }

    public function update(IntBannersRequest $request, $id)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = IntBanner::upload_imagem();

            $registro = IntBanner::where('id', $id)->first();
            $registro->update($input);

            return redirect()->route('painel.internacional.banners.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy($id)
    {
        try {
            $registro = IntBanner::where('id', $id)->first();
            $registro->delete();

            return redirect()->route('painel.internacional.banners.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
