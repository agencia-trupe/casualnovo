<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\MarcasRequest;
use App\Http\Controllers\Controller;

use App\Models\Marca;

class MarcasController extends Controller
{
    public function index()
    {
        if (isset($_GET['local'])) {
            $local = $_GET['local'];
            if ($local == 'brasil') {
                $registros = Marca::brasil()->ordenados()->get();
            }
            if ($local == 'internacional') {
                $registros = Marca::internacional()->ordenados()->get();
            }
        } else {
            $registros = Marca::ordenados()->get();
        }

        return view('painel.marcas.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.marcas.create');
    }

    public function store(MarcasRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Marca::upload_imagem();

            $input['divisao'] = implode(',', $request->get('divisao'));

            Marca::create($input);

            return redirect()->route('painel.marcas.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Marca $registro)
    {
        return view('painel.marcas.edit', compact('registro'));
    }

    public function update(MarcasRequest $request, Marca $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Marca::upload_imagem();

            $input['divisao'] = implode(',', $request->get('divisao'));

            $registro->update($input);

            return redirect()->route('painel.marcas.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Marca $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.marcas.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
