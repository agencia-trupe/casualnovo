<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\BlogPost;
use App\Models\BlogComentario;

class BlogComentariosController extends Controller
{
    public function index(BlogPost $post)
    {
        $registros = $post->comentarios;

        return view('painel.blog.comentarios.index', compact('post', 'registros'));
    }

    public function show(BlogPost $post, BlogComentario $registro)
    {
        return view('painel.blog.comentarios.show', compact('post', 'registro'));
    }

    public function update(Request $request, BlogPost $post, BlogComentario $registro)
    {
        try {

            $registro->update(['aprovado' => $request->get('aprovado')]);

            return redirect()->route('painel.blog.comentarios.index', $post->id)
                ->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(BlogPost $post, BlogComentario $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.blog.comentarios.index', $post)
                ->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
