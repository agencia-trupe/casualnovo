<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosMateriaisRequest;
use App\Http\Controllers\Controller;

use App\Models\ProdutoMaterial;

class ProdutosMateriaisController extends Controller
{
    public function index()
    {
        $registros = ProdutoMaterial::ordenados()->get();

        return view('painel.produtos-materiais.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.produtos-materiais.create');
    }

    public function store(ProdutosMateriaisRequest $request)
    {
        try {

            $input = $request->all();

            ProdutoMaterial::create($input);

            return redirect()->route('painel.produtos-materiais.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(ProdutoMaterial $registro)
    {
        return view('painel.produtos-materiais.edit', compact('registro'));
    }

    public function update(ProdutosMateriaisRequest $request, ProdutoMaterial $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.produtos-materiais.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(ProdutoMaterial $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.produtos-materiais.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
