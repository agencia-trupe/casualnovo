<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\BlogPostsRequest;
use App\Models\BlogCategoria;
use App\Models\BlogPost;
use App\Models\BlogTag;

class BlogPostsController extends Controller
{
    private $categorias;

    public function __construct()
    {
        $this->categorias = BlogCategoria::ordenados()->lists('titulo', 'id');
    }

    public function index(Request $request)
    {
        $categorias = $this->categorias;
        $filtro     = $request->query('filtro');

        if (BlogCategoria::find($filtro)) {
            $registros = BlogPost::where('blog_categorias_id', $filtro)->ordenados()->paginate(25);
        } else {
            if (isset($_GET['local'])) {
                $local = $_GET['local'];
                if ($local == 'brasil') {
                    $registros = BlogPost::brasil()->ordenados()->paginate(25);
                }
                if ($local == 'internacional') {
                    $registros = BlogPost::internacional()->ordenados()->paginate(25);
                }
            } else {
                $registros = BlogPost::ordenados()->paginate(25);
            }
        }

        return view('painel.blog.index', compact('categorias', 'registros', 'filtro'));
    }

    public function create()
    {
        $categorias = $this->categorias;
        $tags = BlogTag::ordenados()->get()->implode('titulo', ',');

        return view('painel.blog.create', compact('categorias', 'tags'));
    }

    public function store(BlogPostsRequest $request)
    {
        try {

            $input = $request->except('tags');

            if (isset($input['capa'])) $input['capa'] = BlogPost::upload_capa();

            $post = BlogPost::create($input);

            $this->syncTags($post, explode(',', $request->get('tags')));

            return redirect()->route('painel.blog.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(BlogPost $registro)
    {
        $categorias = $this->categorias;
        $tags = BlogTag::ordenados()->get()->implode('titulo', ',');

        return view('painel.blog.edit', compact('registro', 'categorias', 'tags'));
    }

    public function update(BlogPostsRequest $request, BlogPost $registro)
    {
        try {

            $input = $request->except('tags');

            if (isset($input['capa'])) $input['capa'] = BlogPost::upload_capa();

            $registro->update($input);

            $this->syncTags($registro, explode(',', $request->get('tags')));

            return redirect()->route('painel.blog.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(BlogPost $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.blog.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }

    protected function syncTags(BlogPost $post, $tags)
    {
        $existingTags = BlogTag::get()->pluck('slug');

        foreach ($tags as $tag) {
            $slug = str_slug($tag);

            if (!$existingTags->contains($slug)) {
                BlogTag::create([
                    'titulo' => $tag,
                    'slug'   => $slug
                ]);
            }
        }

        $tagsSlugs = array_map(function ($tag) {
            return str_slug($tag);
        }, $tags);

        $tagsIds = BlogTag::whereIn('slug', $tagsSlugs)->pluck('id')->toArray();

        $post->tags()->sync($tagsIds);
    }
}
