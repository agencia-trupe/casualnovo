<?php

namespace App\Http\Controllers\Painel;

use App\Http\Requests\ProdutosRequest;
use App\Http\Controllers\Controller;

use App\Models\Fornecedor;
use App\Models\Linha;
use App\Models\Produto;
use App\Models\ProdutoImagem;
use App\Models\Tipo;
use App\Models\ProdutoMaterial;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProdutosController extends Controller
{
    protected $tipos;
    protected $materiais;

    public function __construct()
    {
        $this->tipos = Tipo::ordenados()->lists('titulo', 'id');
        $this->materiais = ProdutoMaterial::ordenados()->lists('titulo', 'id');
    }

    public function index(Fornecedor $fornecedor, Linha $linha)
    {
        if ($linha->fornecedores_id != $fornecedor->id) abort('404');

        $registros = $linha->produtos;

        // dd($registros);

        return view('painel.produtos.index', compact('fornecedor', 'linha', 'registros'));
    }

    public function create(Fornecedor $fornecedor, Linha $linha)
    {
        if ($linha->fornecedores_id != $fornecedor->id) abort('404');

        $tipos = $this->tipos;
        $materiais = $this->materiais;

        return view('painel.produtos.create', compact('fornecedor', 'linha', 'tipos', 'materiais'));
    }

    public function store(ProdutosRequest $request, Fornecedor $fornecedor, Linha $linha)
    {
        if ($linha->fornecedores_id != $fornecedor->id) abort('404');

        try {

            $input = $request->except('materiais', 'imagens', 'origin');

            if (isset($input['imagem_capa'])) $input['imagem_capa'] = Produto::upload_imagem_capa();

            $input['divisao'] = implode(',', $request->get('divisao'));
            $input['linhas_id'] = $linha->id;
            $input['fornecedores_id'] = $fornecedor->id;

            $registro = Produto::create($input);

            $materiais = ($request->get('materiais') ?: []);
            $registro->materiais()->sync($materiais);

            $imagens = $request->get('imagens') ?: [];
            $registro->imagens()->saveMany(array_map(function ($imagem, $index) {
                return new ProdutoImagem([
                    'ordem'  => $index,
                    'imagem' => $imagem,
                ]);
            }, $imagens, array_keys($imagens)));

            return redirect()->route('painel.fornecedores.linhas.produtos.index', [$fornecedor->id, $linha->id])->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Fornecedor $fornecedor, Linha $linha, Produto $registro)
    {
        if ($linha->fornecedores_id != $fornecedor->id) abort('404');

        if ($registro->linhas_id != $linha->id || $registro->fornecedores_id != $fornecedor->id)
            abort('404');

        $tipos = $this->tipos;
        $materiais = $this->materiais;

        $fornecedores = Fornecedor::ordenados()->lists('titulo', 'id');
        $linhas       = $fornecedor->linhas->lists('titulo', 'id');

        return view('painel.produtos.edit', compact('fornecedor', 'linha', 'registro', 'tipos', 'materiais', 'fornecedores', 'linhas'));
    }

    public function update(ProdutosRequest $request, Fornecedor $fornecedor, Linha $linha, Produto $registro)
    {
        try {

            $input = $request->except('materiais', 'imagens', 'origin');

            if (isset($input['imagem_capa'])) $input['imagem_capa'] = Produto::upload_imagem_capa();

            $input['divisao'] = implode(',', $request->get('divisao'));

            if ($input['fornecedores_id'] != $fornecedor->id || $input['linhas_id'] != $linha->id) {
                try {
                    $novoFornecedor = Fornecedor::findOrFail($input['fornecedores_id']);
                    $novaLinha      = Linha::findOrFail($input['linhas_id']);

                    if ($novaLinha->fornecedores_id != $novoFornecedor->id) {
                        throw new \Exception('A Linha selecionada não pertence ao fornecedor.');
                    }
                } catch (\Exception $e) {
                    if ($e instanceof ModelNotFoundException) {
                        throw new \Exception('Fornecedor/linha não encontrado.');
                    }

                    throw new \Exception($e->getMessage());
                }
            }

            $registro->update($input);

            $materiais = ($request->get('materiais') ?: []);
            $registro->materiais()->sync($materiais);

            $imagens = $request->get('imagens') ?: [];
            $registro->imagens()->delete();
            $registro->imagens()->saveMany(array_map(function ($imagem, $index) {
                return new ProdutoImagem([
                    'ordem'  => $index,
                    'imagem' => $imagem,
                ]);
            }, $imagens, array_keys($imagens)));

            if ($request->get('origin')) {
                return redirect($request->get('origin'))->with('success', 'Registro alterado com sucesso.');
            }

            return redirect()->route('painel.fornecedores.linhas.produtos.index', [$registro->fornecedores_id, $registro->linhas_id])->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Fornecedor $fornecedor, Linha $linha, Produto $registro)
    {
        if ($registro->linhas_id != $linha->id || $registro->fornecedores_id !== $fornecedor->id)
            abort('404');

        try {

            $registro->delete();

            return back()->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }

    public function all(Request $request)
    {
        if ($busca = $request->get('busca')) {
            $registros = Produto::with(['fornecedor', 'linha'])
                ->has('fornecedor')
                ->has('linha')
                ->where(function ($q) use ($busca) {
                    $q
                        ->where('codigo', 'LIKE', "%$busca%")
                        ->orWhere('titulo', 'LIKE', "%$busca%");
                })
                ->ordenados()
                ->paginate(50);
        } else {
            if (isset($_GET['local'])) {
                $local = $_GET['local'];
                if ($local == 'brasil') {
                    $registros = Produto::with(['fornecedor', 'linha'])
                        ->has('fornecedor')
                        ->has('linha')
                        ->brasil()
                        ->ordenados()
                        ->paginate(50);
                }
                if ($local == 'internacional') {
                    $registros = Produto::with(['fornecedor', 'linha'])
                        ->has('fornecedor')
                        ->has('linha')
                        ->internacional()
                        ->ordenados()
                        ->paginate(50);
                }
            } else {
                $registros = Produto::with(['fornecedor', 'linha'])
                    ->has('fornecedor')
                    ->has('linha')
                    ->ordenados()
                    ->paginate(50);
            }
        }

        return view('painel.produtos.all', compact('registros'));
    }
}
