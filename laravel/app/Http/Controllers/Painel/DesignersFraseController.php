<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\DesignersFraseRequest;
use App\Http\Controllers\Controller;

use App\Models\DesignersFrase;

class DesignersFraseController extends Controller
{
    public function index()
    {
        $registro = DesignersFrase::first();

        return view('painel.designers-frase.edit', compact('registro'));
    }

    public function update(DesignersFraseRequest $request, DesignersFrase $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.designers-frase.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
