<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\IntPerfilRequest;
use App\Models\IntPerfil;

class IntPerfilController extends Controller
{
    public function index()
    {
        $registro = IntPerfil::first();

        return view('painel.internacional.perfil.edit', compact('registro'));
    }

    public function update(IntPerfilRequest $request, $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = IntPerfil::upload_imagem();

            $registro = IntPerfil::where('id', $registro->id)->first();
            $registro->update($input);

            return redirect()->route('painel.internacional.perfil.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
