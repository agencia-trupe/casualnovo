<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EnderecosRequest;
use App\Http\Controllers\Controller;

use App\Models\Endereco;

class EnderecosController extends Controller
{
    public function index()
    {
        $registros = Endereco::ordenados()->get();

        return view('painel.enderecos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.enderecos.create');
    }

    public function store(EnderecosRequest $request)
    {
        try {

            $input = $request->all();

            Endereco::create($input);

            return redirect()->route('painel.enderecos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Endereco $registro)
    {
        return view('painel.enderecos.edit', compact('registro'));
    }

    public function update(EnderecosRequest $request, Endereco $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.enderecos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Endereco $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.enderecos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
