<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\DesignersImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Designer;
use App\Models\DesignerImagem;

use App\Helpers\CropImage;

class DesignersImagensController extends Controller
{
    public function index(Designer $registro)
    {
        $imagens = DesignerImagem::designer($registro->id)->ordenados()->get();

        return view('painel.designers.imagens.index', compact('imagens', 'registro'));
    }

    public function show(Designer $registro, DesignerImagem $imagem)
    {
        return $imagem;
    }

    public function store(Designer $registro, DesignersImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = DesignerImagem::uploadImagem();
            $input['designer_id'] = $registro->id;

            $imagem = DesignerImagem::create($input);

            $view = view('painel.designers.imagens.imagem', compact('registro', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Designer $registro, DesignerImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.designers.imagens.index', $registro)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Designer $registro)
    {
        try {

            $registro->imagens()->delete();
            return redirect()->route('painel.designers.imagens.index', $registro)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
