<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Produto;
use App\Models\Tipo;

class ProdutosController extends Controller
{
    public function tipo($tipo_slug, $codigo = null)
    {
        $tipo = Tipo::findBySlugOrFail($tipo_slug);
        $produtos = Produto::site()->brasil()->where('tipos_id', $tipo->id);

        if ($divisao = request('divisao')) {
            $produtos = $produtos->where('divisao', 'LIKE', "%$divisao%");
        }

        $produtos = $produtos->ordenados()->get();

        if (! $codigo) {
            return view('frontend.catalogo.index', compact('tipo', 'produtos'));
        }

        $produto = $produtos->where('codigo', $codigo)->first();

        if (! $produto) {
            abort('404');
        }

        return view('frontend.catalogo.show', compact('tipo', 'produtos', 'produto'));
    }

    public function busca(Request $request)
    {
        $codigo = $request->get('codigo');
        $divisao = $request->get('divisao');

        if (!$codigo) {
            abort('404');
        }

        $produtos = Produto::site()->brasil()->has('tipo');

        if ($codigo) {
            $produtos = $produtos->where('codigo', 'LIKE', "%$codigo%");
        }
        if ($divisao) {
            $produtos = $produtos->where('divisao', 'LIKE', "%$divisao%");
        }

        $produtos = $produtos->get();

        return view('frontend.catalogo.busca', compact('produtos'));
    }
}
