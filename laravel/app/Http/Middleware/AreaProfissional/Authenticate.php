<?php

namespace App\Http\Middleware\AreaProfissional;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'profissional')
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('area-profissional/login');
            }
        } elseif (Auth::guard($guard)->user()->bloqueado) {
            Auth::guard('profissional')->logout();
            return redirect()->guest('area-profissional/login')->with('error', 'Usuário bloqueado');
        }

        return $next($request);
    }
}
