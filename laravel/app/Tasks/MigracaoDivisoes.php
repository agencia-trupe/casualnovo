<?php

namespace App\Tasks;

use App\Models\Fornecedor;
use App\Models\Produto;

class MigracaoDivisoes {
    private $fornecedores = [
        'conceito' => [
            25, // ARMANI/CASA
            57, // CARLO MORETTI
            59, // DRIADE
            62, // VENINI
            66, // GIOBAGNARA
            55, // ARCAHORN
            56, // BITOSSI
            61, // GINGER BROWN
            132, // SEGUSO GIANNI
            60, // GILLES CAFIER
            94, // ARCADE
            58, // DE NACRE ET D'ORIENT
            63, // JONATHAN ADLER
            68, // MOSE -DANIELA BUSARELO
            127, // SEGVSO
            65, // BAROVIER &TOSO
            72, // GUAXS
            100, // VERREUM
            78, // MUNNA
            89, // RINA MENARDI
            101, // KOSE
            98, // PULPO
            161, // RALPH LAUREN
            166, // SAINT LOUIS
            119, // HB- HEDWIIG -BOLLHAGEN
            121, // BOREALIS
            123, // SINERGIA -Jasmina Ajzenkol
            126, // BOCA DO LOBO
            125, // KELLY WEARSTLER
            130, // DELIGHFULL
            131, // ESSENTIAL HOME
            134, // ByKOKET
            133, // BRABBU
            145, // LUXXU
            147, // SKLO
            148, // RUBELLI
            149, // DONGHIA
            152, // ECART
            156, // LIAIGRE
            157, // SALVATORI
            158, // R&Y AUGOUSTI
        ],

        'projetos' => [
            96, // RIMADESIO
            113, // OIKOS
            114, // LUALDI
            110, // POLIFORM COZINHA
        ]
    ];

    private function substituiDivisao($produto, $slug) {
        $divisoes = explode(',', $produto->divisao);
        $divisoes[] = $slug;

        $produto->update([
            'divisao' => implode(',', array_unique(array_filter($divisoes)))
        ]);
    }

    private function alteraProdutos($fornecedores, $divisao) {
        $produtos = Produto::whereIn('fornecedores_id', $fornecedores)->get();
        foreach ($produtos as $produto) {
            $this->substituiDivisao($produto, $divisao);
        }
    }

    private function alteraFornecedores(array $fornecedoresIds, $divisao) {
        $fornecedores = Fornecedor::whereIn('id', $fornecedoresIds)->get();
        foreach($fornecedores as $fornecedor) {
            $this->substituiDivisao($fornecedor, $divisao);
        }
    }

    public function run() {
        foreach ($this->fornecedores as $divisao => $fornecedores) {
            $this->alteraFornecedores($fornecedores, $divisao);
            $this->alteraProdutos($fornecedores, $divisao);
        }
    }
}
