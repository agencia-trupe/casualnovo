<?php

namespace App\Helpers;

use Carbon\Carbon;

class Tools
{
    public static function routeIs($routeNames)
    {
        foreach ((array) $routeNames as $routeName) {
            if (str_is($routeName, \Route::currentRouteName())) {
                return true;
            }
        }

        return false;
    }

    public static function fileUpload($input, $path = 'arquivos/')
    {
        if (! $file = request()->file($input)) {
            throw new \Exception("O campo (${input}) não contém nenhum arquivo.", 1);
        }

        $fileName  = str_slug(
            pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)
        );
        $fileName .= '_'.date('YmdHis');
        $fileName .= str_random(10);
        $fileName .= '.'.$file->getClientOriginalExtension();

        $file->move(public_path($path), $fileName);

        return $fileName;
    }

    public static function parseLink($url)
    {
        return parse_url($url, PHP_URL_SCHEME) === null ? 'http://'.$url : $url;
    }

    public static function mesExtenso($mes)
    {
        $meses = [
            'pt' => ['janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro'],
            'en' => ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december']
        ];

        return $meses[app()->getLocale()][$mes - 1];
    }

    public static function dataBlog($data)
    {
        $d = Carbon::createFromFormat('d/m/Y', $data);

        return
            $d->format('d').' '.
            substr(mb_strtoupper(self::mesExtenso($d->format('m'))), 0, 3).' '.
            $d->format('Y');
    }

    public static function catalogoLink()
    {
        return route('area-profissional');
    }

    public static function listaPaises()
    {
        $paises = [
            ['Afeganistão', 'Afghanistan'],
            ['Andorra', 'Andorra'],
            ['Angola', 'Angola'],
            ['Antígua e Barbuda', 'Antigua e Barbuda'],
            ['Argélia', 'Algeria'],
            ['Argentina', 'Argentina'],
            ['Armênia', 'Armenia'],
            ['Austrália', 'Australia'],
            ['Áustria', 'Austria'],
            ['Azerbaijão', 'Azerbaijan'],
            ['Bahamas', 'The Bahamas'],
            ['Bangladesh', 'Bangladesh'],
            ['Barbados', 'Barbados'],
            ['Barém', 'Bahrain'],
            ['Bielorrússia', 'Belarus'],
            ['Bélgica', 'Belgium'],
            ['Belize', 'Belize'],
            ['Benim', 'Benin'],
            ['Bolívia', 'Bolivia'],
            ['Bósnia; Bósnia e Herzegovina', 'Bosnia; Bosnia and Herzegovina'],
            ['Botsuana', 'Botswana'],
            ['Brasil', 'Brazil'],
            ['Brunei', 'Brunei'],
            ['Bulgária', 'Bulgaria'],
            ['BurkinaFaso', 'BurkinaFaso'],
            ['Burundi', 'Burundi'],
            ['Butão', 'Bhutan'],
            ['Cabo Verde', 'Cape Verde'],
            ['Camarões', 'Cameroon'],
            ['Camboja', 'Cambodia'],
            ['Canadá', 'Canada'],
            ['República Centro-Africana', 'Central African Republic'],
            ['Chade', 'Chad'],
            ['China', 'China'],
            ['Chile', 'Chile'],
            ['Ilhas Cook', 'Cook Islands'],
            ['Colômbia', 'Colombia'],
            ['Comores', 'Comoros'],
            ['Costa Rica', 'Costa Rica'],
            ['Croácia', 'Croatia'],
            ['Cuba', 'Cuba'],
            ['Chipre', 'Cyprus'],
            ['República Tcheca', 'Czech Republic'],
            ['República Democrática do Congo', 'Democratic Republic of Congo'],
            ['Dinamarca', 'Denmark'],
            ['Djibuti', 'Djibouti'],
            ['Dominica', 'Dominica'],
            ['República Dominicana', 'Dominican Republic'],
            ['Timor Leste', 'East Timor'],
            ['Equador', 'Ecuador'],
            ['Egito', 'Egypt'],
            ['El Salvador', 'El Salvador'],
            ['Inglaterra', 'England'],
            ['Guiné Equatorial', 'Equatorial Guinea'],
            ['Eritreia', 'Eritrea'],
            ['Estônia', 'Estônia'],
            ['Fiji', 'Fiji'],
            ['Finlândia', 'Finland'],
            ['França', 'France'],
            ['Gabão', 'Gabon'],
            ['Gâmbia', 'Gambia'],
            ['Geórgia', 'Georgia'],
            ['Alemanha', 'Germany'],
            ['Granada', 'Grenada'],
            ['Grécia', 'Greece'],
            ['Guatemala', 'Guatemala'],
            ['Guiné', 'Guinea'],
            ['GuinéBissau', 'Guinea–Bissau'],
            ['Guiana', 'Guyana'],
            ['Haiti', 'Haiti'],
            ['Holanda', 'Holland'],
            ['Honduras', 'Honduras'],
            ['Hungria', 'Hungary'],
            ['Islândia', 'Iceland'],
            ['Índia', 'India'],
            ['Indonésia', 'Indonesia'],
            ['Irã', 'Iran'],
            ['Irlanda', 'Ireland'],
            ['Israel', 'Israel'],
            ['Itália', 'Italy'],
            ['Costa do Marfim', 'Ivory Coast'],
            ['Jamaica', 'Jamaica'],
            ['Japão', 'Japan'],
            ['Jordânia', 'Jordan'],
            ['Cazaquistão', 'Kazakhstan'],
            ['Quênia', 'Kenya'],
            ['Quiribati', 'Kiribati'],
            ['Quirguistão', 'Kyrgyzstan'],
            ['Kuwait', 'Kwait'],
            ['Laos', 'Laos'],
            ['Letônia', 'Latvia'],
            ['Líbano', 'Lebanon'],
            ['Lesoto', 'Lesotho'],
            ['Libéria', 'Liberia'],
            ['Liechtenstein', 'Liechtenstein'],
            ['Lituânia', 'Lithuania'],
            ['Luxemburgo', 'Luxembourg'],
            ['Líbia', 'Lybia'],
            ['Macedônia', 'Macedonia'],
            ['Madagascar', 'Madagascar'],
            ['Malásia', 'Malaysia'],
            ['Malaui', 'Malawi'],
            ['Maldivas', 'Maldives'],
            ['Máli', 'Mali'],
            ['Malta', 'Malta'],
            ['Maurício', 'Mauritius'],
            ['Mauritânia', 'Mauritia'],
            ['Ilhas Marshall', 'Marshall Island'],
            ['Estados Federados da Micronésia', 'Micronesia/Federated States of Micronesia'],
            ['México', 'Mexico'],
            ['Marrocos', 'Morocco'],
            ['Moldavia', 'Moldova'],
            ['Mônaco', 'Monaco'],
            ['Mongólia', 'Mongolia'],
            ['Montenegro', 'Montenegro'],
            ['Moçambique', 'Mozambique'],
            ['Myanmar', 'Myanmar'],
            ['Namíbia', 'Namibia'],
            ['Nauru', 'Nauru'],
            ['Nepal', 'Nepal'],
            ['Nova Zelândia', 'New Zealand'],
            ['Nicarágua', 'Nicaragua'],
            ['Níger', 'Niger'],
            ['Nigéria', 'Nigeria'],
            ['Niue', 'Niue'],
            ['Coréia do Norte', 'North Korea'],
            ['Noruega', 'Norway'],
            ['Omã', 'Oman'],
            ['Palestina', 'Palestine'],
            ['Paquistão', 'Pakistan'],
            ['Palau', 'Palau'],
            ['Panamá', 'Panama'],
            ['Papua Nova Guiné', 'Papua New Guinea'],
            ['Paraguai', 'Paraguay'],
            ['Peru', 'Peru'],
            ['Philippines', 'Philippines'],
            ['Polônia', 'Poland'],
            ['Portugal', 'Portugal'],
            ['Catar', 'Qatar'],
            ['Romênia', 'Romania'],
            ['Rússia', 'Russia'],
            ['Ruanda', 'Rwanda'],
            ['Samoa', 'Samoa'],
            ['Santa Lúcia', 'Saint Lucia'],
            ['São Cristóvão e Nevis', 'Saint Kitts and Nevis'],
            ['São Marino', 'San Marino'],
            ['São Tomé e Príncipe', 'Sao Tomé and Principe'],
            ['São Vicente e Granadinas', 'Saint Vincent and the Grenadines'],
            ['Escócia', 'Scotland'],
            ['Senegal', 'Senegal'],
            ['Sérvia', 'Serbia'],
            ['Seicheles', 'Seychelles'],
            ['Serra Leoa', 'Sierra Leone'],
            ['Singapura', 'Singapore'],
            ['Eslováquia', 'Slovakia'],
            ['Ilhas Salomão', 'Solomon Islands'],
            ['Somália', 'Somalia'],
            ['África do Sul', 'South Africa'],
            ['Coréia do Sul', 'South Korea'],
            ['Sudão do Sul', 'South Sudan'],
            ['Espanha', 'Spain'],
            ['Sri Lanka', 'Sri Lanka'],
            ['Sudão', 'Sudan'],
            ['Suriname', 'Suriname'],
            ['Suazilândia', 'Swaziland'],
            ['Suécia', 'Sweden'],
            ['Suíça', 'Switzerland'],
            ['Síria', 'Syria'],
            ['Tadiquistão', 'Tajikistan'],
            ['Tanzânia', 'Tanzania'],
            ['Tailândia', 'Thailand'],
            ['Togo', 'Togo'],
            ['Tonga', 'Tonga'],
            ['Trindade e Tobago', 'Trinidad and Tobago'],
            ['Tunísia', 'Tunisia'],
            ['Turcomenistão', 'Turkmenistan'],
            ['Turquia', 'Turkey'],
            ['Tuvalu', 'Tuvalu'],
            ['Ucrânia', 'Ukraine'],
            ['Uganda', 'Uganda'],
            ['Uruguai', 'Uruguay'],
            ['Emirados Árabes Unidos', 'United Arab Emirates'],
            ['Reino Unido', 'United Kingdom'],
            ['Estados Unidos', 'United States of America'],
            ['Usbequistão', 'Uzbekistan'],
            ['Vanuatu', 'Vanuatu'],
            ['Venezuela', 'Venezuela'],
            ['Vietnã', 'Vietnam'],
            ['País de Gales', 'Wales'],
            ['Iêmen', 'Yemen'],
            ['Zâmbia', 'Zambia'],
            ['Zimbábue', 'Zimbabwe'],
        ];

        $keyLocalizacao = app()->getLocale() == 'pt' ? 0 : 1;

        $paisesLocalizados = [];
        foreach($paises as $pais) {
            $paisesLocalizados[$pais[0]] = $pais[$keyLocalizacao];
        }

        asort($paisesLocalizados);

        return $paisesLocalizados;
    }
}
