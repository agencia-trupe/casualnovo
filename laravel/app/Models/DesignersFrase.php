<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class DesignersFrase extends Model
{
    protected $table = 'designers_frase';

    protected $guarded = ['id'];

}
