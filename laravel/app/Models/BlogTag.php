<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogTag extends Model
{
    protected $table = 'blog_tags';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('titulo', 'ASC');
    }

    public function posts()
    {
        return $this->belongsToMany(BlogPost::class, 'blog_tags_has_blog_posts', 'blog_tags_id', 'blog_posts_id');
    }
}
