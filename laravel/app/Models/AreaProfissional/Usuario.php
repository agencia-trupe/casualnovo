<?php

namespace App\Models\AreaProfissional;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Usuario extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    protected $table = 'usuarios_catalogo';

    protected $guarded = ['id'];

    protected $hidden = ['password', 'remember_token'];

    public function getPodeSolicitarOrcamentoAttribute()
    {
        return strtolower($this->pais) !== 'brasil';
    }

    public function getParticipaProgramaRelacionamentoAttribute()
    {
        return $this->participante_relacionamento && ProgramaCampanhas::ativa()->first();
    }

    public function pontuacao($id_campanha = null)
	{
		if (is_null($id_campanha)) {
			$campanha = ProgramaCampanhas::ativa()->first();

			if(!is_null($campanha)) $id_campanha = $campanha->id;
		}

        return $this
            ->hasMany(ProgramaPontuacao::class, 'usuarios_catalogo_id')
            ->where('programa_campanha_id', '=', $id_campanha)
            ->orderBy('data_insercao', 'desc');
	}

    public function pontuacaoTotal($id_campanha = null, $formatar = true)
    {
		if(is_null($id_campanha)) {
			$campanha = ProgramaCampanhas::ativa()->first();
        } else {
            $campanha = ProgramaCampanhas::find($id_campanha);
        }

		if (is_null($campanha)) return '0';

		$pontos = $this->pontuacao($campanha->id)->get();

		$total = 0.0;
		foreach ($pontos as $key => $value) {
			$total = $total + ($value->getOriginal('pontos') / 100);
		}

		if($formatar) {
			setLocale(LC_MONETARY, 'pt_BR.utf8', 'pt_BR');
			return money_format('%!i', $total);
		} else {
			return $total;
		}
	}
}
