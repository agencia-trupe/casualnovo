<?php

namespace App\Models\AreaProfissional;

use Illuminate\Database\Eloquent\Model;

class ProgramaPremios extends Model
{
    protected $table = 'programa_premios';

    protected $guarded = ['id'];

    public function scopePontos($query, $pontuacao){
        return $query
            ->where('inicio_pontuacao', '<=', $pontuacao * 100)
            ->where('fim_pontuacao', '>=', $pontuacao * 100);
    }

    public function getInicioPontuacaoAttribute($value)
    {
    	setLocale(LC_MONETARY, 'pt_BR.utf8', 'pt_BR');
        return money_format('%!i', (float) $value / 100);
    }

    public function getFimPontuacaoAttribute($value)
    {
    	setLocale(LC_MONETARY, 'pt_BR.utf8', 'pt_BR');
        return money_format('%!i', (float) $value / 100);
    }
}
