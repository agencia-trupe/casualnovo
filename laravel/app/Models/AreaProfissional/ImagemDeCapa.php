<?php

namespace App\Models\AreaProfissional;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ImagemDeCapa extends Model
{
    protected $table = 'imagem_de_capa';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 1980,
            'height' => null,
            'path'   => 'assets/img/imagem-de-capa/'
        ]);
    }
}
