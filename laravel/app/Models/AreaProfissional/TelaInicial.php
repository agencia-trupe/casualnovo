<?php

namespace App\Models\AreaProfissional;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class TelaInicial extends Model
{
    protected $table = 'tela_inicial';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 800,
            'height' => null,
            'path'   => 'assets/img/tela-inicial/'
        ]);
    }

    public static function upload_imagem_en()
    {
        return CropImage::make('imagem_en', [
            'width'  => 800,
            'height' => null,
            'path'   => 'assets/img/tela-inicial/'
        ]);
    }

}
