<?php

namespace App\Models\AreaProfissional;

use Illuminate\Database\Eloquent\Model;

use App\Models\Produto;

class Orcamento extends Model
{
    protected $table = 'orcamentos';

    protected $guarded = ['id'];

    public function usuario()
    {
        return $this->belongsTo(Usuario::class, 'usuario_id');
    }

    public function getDataAttribute()
    {
        return $this->created_at->format('d/m/Y - H:i');
    }

    public function getItensAttribute()
    {
        $itens = collect(json_decode($this->orcamento));

        $produtosIds = $itens->map(function($item) {
            return $item->id;
        });

        $produtos = Produto::with('tipo')->whereIn('id', $produtosIds)->get();

        return $itens->map(function($item) use ($produtos) {
            $item->quantidade = property_exists($item, 'quantidade') ? $item->quantidade : 1;
            $item->produto = $produtos->first(function($index, $produto) use ($item) {
                return $produto->id == $item->id;
            });

            return $item;
        });
    }

    public function getItensCountAttribute()
    {
        return count(json_decode($this->orcamento, true));
    }
}
