<?php

namespace App\Models\AreaProfissional;

use Illuminate\Database\Eloquent\Model;

class EMailAvisos extends Model
{
    protected $table = 'e_mail_avisos';

    protected $guarded = ['id'];
}
