<?php

namespace App\Models\AreaProfissional;

use Illuminate\Database\Eloquent\Model;

class ProgramaCampanhas extends Model
{
    protected $table = 'programa_campanha';

    protected $guarded = ['id'];

    public function scopeAtiva($query)
    {
        $data = Date('Y-m-d');

        return $query
            ->where('data_inicio', '<=', $data)
            ->where('data_termino', '>=', $data);
    }

    public function premios($tipo = 'arquiteto')
    {
        return $this
            ->hasMany(ProgramaPremios::class, 'programa_campanha_id')
            ->where('tipo', '=', $tipo)
            ->orderBy('inicio_pontuacao', 'asc');
    }
}
