<?php

namespace App\Models\AreaProfissional;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProgramaPontuacao extends Model
{
    use SoftDeletes;

    protected $table = 'programa_pontuacao';

    protected $guarded = ['id'];

    public function getPontosAttribute($value)
    {
    	setLocale(LC_MONETARY, 'pt_BR.utf8', 'pt_BR');
        return money_format('%!i', (float) $value / 100);
    }
}
