<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Chamada extends Model
{
    protected $table = 'chamadas';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeBrasil($query)
    {
        return $query->where('brasil', '=', 1);
    }

    public function scopeInternacional($query)
    {
        return $query->where('internacional', '=', 1);
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 800,
            'height' => 512,
            'path'   => 'assets/img/chamadas/'
        ]);
    }
}
