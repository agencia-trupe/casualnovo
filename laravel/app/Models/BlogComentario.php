<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogComentario extends Model
{
    protected $table = 'blog_comentarios';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('created_at', 'ASC');
    }

    public function post()
    {
        return $this->belongsTo(BlogPost::class, 'blog_posts_id');
    }
}
