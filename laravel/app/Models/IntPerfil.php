<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class IntPerfil extends Model
{
    protected $table = 'int_perfil';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 610,
            'height' => null,
            'path'   => 'assets/img/internacional/perfil/'
        ]);
    }
}
