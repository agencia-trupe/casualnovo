<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class DesignerImagem extends Model
{
    protected $table = 'designers_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeDesigner($query, $id)
    {
        return $query->where('designer_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 420,
                'height'  => 300,
                'path'    => 'assets/img/designers/imagens/thumbs/'
            ],
            [
                'width'   => 900,
                'height'  => null,
                'upsize'  => true,
                'path'    => 'assets/img/designers/imagens/'
            ]
        ]);
    }
}
