<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Fornecedor extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'fornecedores';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeBrasil($query)
    {
        return $query->where('brasil', '=', 1);
    }

    public function scopeInternacional($query)
    {
        return $query->where('internacional', '=', 1);
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 120,
            'height' => 50,
            'color'  => '#fff',
            'path'   => 'assets/img/fornecedores/'
        ]);
    }

    public function linhas()
    {
        return $this->hasMany(Linha::class, 'fornecedores_id')->ordenados();
    }
}
