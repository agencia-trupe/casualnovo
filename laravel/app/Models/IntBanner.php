<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class IntBanner extends Model
{
    protected $table = 'int_banners';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 1920,
            'height' => null,
            'path'   => 'assets/img/internacional/banners/'
        ]);
    }
}
