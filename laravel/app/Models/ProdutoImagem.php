<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ProdutoImagem extends Model
{
    protected $table = 'produtos_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeProduto($query, $id)
    {
        return $query->where('produtos_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 220,
                'height'  => 220,
                'path'    => 'assets/images/produtos/capas/'
            ],
            [
                'width'   => null,
                'height'  => null,
                'path'    => 'assets/images/produtos/originais/'
            ],
            [
                'width'   => 890,
                'height'  => null,
                'path'    => 'assets/images/produtos/redimensionadas/'
            ],
            [
                'width'   => null,
                'height'  => 120,
                'path'    => 'assets/images/produtos/catalogo/'
            ]
        ]);
    }
}
