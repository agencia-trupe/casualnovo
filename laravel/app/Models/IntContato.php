<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IntContato extends Model
{
    protected $table = 'int_contato';

    protected $guarded = ['id'];
}
