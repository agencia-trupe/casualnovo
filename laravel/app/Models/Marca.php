<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Marca extends Model
{
    protected $table = 'marcas';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeBrasil($query)
    {
        return $query->where('brasil', '=', 1);
    }

    public function scopeInternacional($query)
    {
        return $query->where('internacional', '=', 1);
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => null,
            'height' => 60,
            'path'   => 'assets/img/marcas/'
        ]);
    }
}
