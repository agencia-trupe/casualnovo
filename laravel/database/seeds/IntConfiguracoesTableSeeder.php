<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class IntConfiguracoesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('int_configuracoes')->insert([
            'title' => 'Casual Furniture',
        ]);
    }
}
