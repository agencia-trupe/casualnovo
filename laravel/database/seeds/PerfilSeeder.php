<?php

use Illuminate\Database\Seeder;

class PerfilSeeder extends Seeder
{
    public function run()
    {
        DB::table('perfil')->insert([
            'visao' => '',
            'visao_en' => '',
            'valores' => '',
            'valores_en' => '',
            'missao' => '',
            'missao_en' => '',
            'imagem' => '',
        ]);
    }
}
