<?php

use Illuminate\Database\Seeder;

class TelaInicialSeeder extends Seeder
{
    public function run()
    {
        DB::table('tela_inicial')->insert([
            'texto' => '',
            'texto_en' => '',
            'imagem' => '',
            'imagem_en' => '',
        ]);
    }
}
