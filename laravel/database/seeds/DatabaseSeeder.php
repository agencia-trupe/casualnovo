<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
		$this->call(TelaInicialSeeder::class);
		$this->call(EMailAvisosSeeder::class);
		$this->call(ImagemDeCapaSeeder::class);
		$this->call(HomeSeeder::class);
		$this->call(PerfilSeeder::class);
		$this->call(DesignersFraseSeeder::class);
		$this->call(ConfiguracoesSeeder::class);
        $this->call(ContatoTableSeeder::class);
        $this->call(PoliticaDePrivacidadeTableSeeder::class);
        $this->call(IntContatoTableSeeder::class);
        $this->call(IntConfiguracoesTableSeeder::class);
        $this->call(IntHomeTableSeeder::class);
        $this->call(IntPerfilTableSeeder::class);

        Model::reguard();
    }
}
