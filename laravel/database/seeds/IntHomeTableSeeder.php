<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class IntHomeTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('int_home')->insert([
            'imagem' => '',
            'link' => '',
            'titulo_en' => '',
            'titulo_es' => '',
            'imagem_2' => '',
            'link_2' => '',
            'titulo_2_en' => '',
            'titulo_2_es' => '',
        ]);
    }
}
