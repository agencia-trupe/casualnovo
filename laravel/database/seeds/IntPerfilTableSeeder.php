<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class IntPerfilTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('int_perfil')->insert([
            'visao_en'   => '',
            'visao_es'   => '',
            'valores_en' => '',
            'valores_es' => '',
            'missao_en'  => '',
            'missao_es'  => '',
            'imagem'     => '',
            'texto_en'   => '',
            'texto_es'   => '',
        ]);
    }
}
