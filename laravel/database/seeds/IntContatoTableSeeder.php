<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class IntContatoTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('int_contato')->insert([
            'email' => 'contato@trupe.net',
        ]);
    }
}
