<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerfilTable extends Migration
{
    public function up()
    {
        Schema::create('perfil', function (Blueprint $table) {
            $table->increments('id');
            $table->text('visao');
            $table->text('visao_en');
            $table->text('valores');
            $table->text('valores_en');
            $table->text('missao');
            $table->text('missao_en');
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('perfil');
    }
}
