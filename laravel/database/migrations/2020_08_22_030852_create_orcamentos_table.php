<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrcamentosTable extends Migration
{
    public function up()
    {
        Schema::create('orcamentos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('usuario_id')->unsigned()->nullable();
            $table->foreign('usuario_id')->references('id')->on('usuarios_catalogo')->onDelete('set null');
            $table->json('orcamento');
            $table->text('observacoes');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('orcamentos');
    }
}
