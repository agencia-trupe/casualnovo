<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTelaInicialTable extends Migration
{
    public function up()
    {
        Schema::create('tela_inicial', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->text('texto_en');
            $table->string('imagem');
            $table->string('imagem_en');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('tela_inicial');
    }
}
