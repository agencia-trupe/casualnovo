<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogTables extends Migration
{
    public function up()
    {
        Schema::create('blog_categorias', function(Blueprint $table) {
            $table->increments('id');
            $table->string('titulo', 250)->nullable();
            $table->string('titulo_en', 250)->nullable();
            $table->string('slug', 250)->nullable();
            $table->integer('ordem')->nullable();
            $table->timestamps();
        });

        Schema::create('blog_posts', function(Blueprint $table) {
            $table->increments('id');
            $table->string('titulo', 250)->nullable();
            $table->string('titulo_en', 250)->nullable();
            $table->string('slug', 250)->nullable();
            $table->string('capa', 250)->nullable();
            $table->date('data')->nullable();
            $table->text('texto')->nullable();
            $table->text('texto_en')->nullable();
            $table->integer('publicar')->nullable();
            $table->integer('blog_categorias_id')->unsigned()->nullable();
            $table->foreign('blog_categorias_id')->references('id')->on('blog_categorias')->onDelete('CASCADE');
            $table->timestamps();
        });

        Schema::create('blog_comentarios', function(Blueprint $table) {
            $table->increments('id');
            $table->string('autor', 250)->nullable();
            $table->string('email', 250)->nullable();
            $table->date('data')->nullable();
            $table->text('texto')->nullable();
            $table->integer('aprovado')->nullable();
            $table->integer('blog_posts_id')->unsigned()->nullable();
            $table->foreign('blog_posts_id')->references('id')->on('blog_posts')->onDelete('CASCADE');
            $table->timestamps();
        });

        Schema::create('blog_tags', function(Blueprint $table) {
            $table->increments('id');
            $table->string('titulo', 250)->nullable();
            $table->string('slug', 250)->nullable();
            $table->integer('ordem')->nullable();
            $table->timestamps();
        });

        Schema::create('blog_tags_has_blog_posts', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('blog_tags_id')->unsigned()->nullable();
            $table->foreign('blog_tags_id')->references('id')->on('blog_tags')->onDelete('CASCADE');
            $table->integer('blog_posts_id')->unsigned()->nullable();
            $table->foreign('blog_posts_id')->references('id')->on('blog_posts')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('blog_categorias');
		Schema::drop('blog_posts');
		Schema::drop('blog_comentarios');
		Schema::drop('blog_tags');
		Schema::drop('blog_tags_has_blog_posts');
    }
}
