<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateIntHomeTable extends Migration
{
    public function up()
    {
        Schema::create('int_home', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem');
            $table->string('link');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->string('imagem_2');
            $table->string('link_2');
            $table->string('titulo_2_en');
            $table->string('titulo_2_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('int_home');
    }
}
