<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPerfilTable extends Migration
{
    public function up()
    {
        Schema::table('perfil', function(Blueprint $table) {
            $table->dropColumn('visao');
            $table->dropColumn('visao_en');
            $table->dropColumn('valores');
            $table->dropColumn('valores_en');
            $table->dropColumn('missao');
            $table->dropColumn('missao_en');
            $table->text('texto')->after('id');
            $table->text('texto_en')->after('texto');
        });
    }

    public function down()
    {
        Schema::table('perfil', function(Blueprint $table) {
            $table->text('missao_en')->after('id');
            $table->text('missao')->after('id');
            $table->text('valores_en')->after('id');
            $table->text('valores')->after('id');
            $table->text('visao_en')->after('id');
            $table->text('visao')->after('id');
            $table->dropColumn('texto');
            $table->dropColumn('texto_en');
        });
    }
}
