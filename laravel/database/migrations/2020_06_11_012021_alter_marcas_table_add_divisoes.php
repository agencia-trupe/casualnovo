<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMarcasTableAddDivisoes extends Migration
{
    public function up()
    {
        Schema::table('marcas', function(Blueprint $table) {
            $table->string('divisao')->after('ordem');
        });
    }

    public function down()
    {
        Schema::table('marcas', function(Blueprint $table) {
            $table->dropColumn('divisao');
        });
    }
}
