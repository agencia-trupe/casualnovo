<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterContatoTableRemoveEnderecos extends Migration
{
    public function up()
    {
        Schema::table('contato', function(Blueprint $table) {
            $table->dropColumn('enderecos');
            $table->dropColumn('enderecos_en');
            $table->dropColumn('google_maps');
        });
    }

    public function down()
    {
        Schema::table('contato', function(Blueprint $table) {
            $table->text('enderecos')->after('twitter');
            $table->text('enderecos_en')->after('enderecos');
            $table->text('google_maps')->after('enderecos_en');
        });
    }
}
