<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosMateriaisTable extends Migration
{
    public function up()
    {
        Schema::create('produtos_materiais', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('titulo');
            $table->string('titulo_en');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('produtos_materiais');
    }
}
