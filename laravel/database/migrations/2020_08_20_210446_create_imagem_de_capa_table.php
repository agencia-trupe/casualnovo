<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagemDeCapaTable extends Migration
{
    public function up()
    {
        Schema::create('imagem_de_capa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('imagem_de_capa');
    }
}
