<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterHomeTable extends Migration
{
    public function up()
    {
        Schema::table('home', function(Blueprint $table) {
            $table->string('titulo');
            $table->string('titulo_en');
            $table->string('titulo_2');
            $table->string('titulo_2_en');
            $table->string('imagem_2');
            $table->string('link_2');
        });
    }

    public function down()
    {
        Schema::table('home', function(Blueprint $table) {
            $table->dropColumn('titulo');
            $table->dropColumn('titulo_en');
            $table->dropColumn('titulo_2');
            $table->dropColumn('titulo_2_en');
            $table->dropColumn('imagem_2');
            $table->dropColumn('link_2');
        });
    }
}
