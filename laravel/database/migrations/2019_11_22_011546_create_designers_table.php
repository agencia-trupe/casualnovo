<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignersTable extends Migration
{
    public function up()
    {
        Schema::create('designers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('nome');
            $table->string('foto');
            $table->text('texto');
            $table->text('texto_en');
            $table->timestamps();
        });

        Schema::create('designers_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('designer_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('designer_id')->references('id')->on('designers')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('designers_imagens');
        Schema::drop('designers');
    }
}
