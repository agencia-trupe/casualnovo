<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFornecedoresTable extends Migration
{
    public function up()
    {
        Schema::create('fornecedores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('divisao');
            $table->string('titulo');
            $table->string('imagem');
            $table->text('obs');
            $table->integer('publicar_site')->nullable();
            $table->integer('publicar_catalogo')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('fornecedores');
    }
}
