<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsuariosCatalogoTable extends Migration
{
    public function up()
    {
        Schema::table('usuarios_catalogo', function(Blueprint $table) {
            $table->string('pais')->default('brasil');
            $table->boolean('receber_newsletter')->default(true);
            $table->boolean('bloqueado')->default(false);
        });
    }

    public function down()
    {
        Schema::table('usuarios_catalogo', function(Blueprint $table) {
            $table->dropColumn('pais');
            $table->dropColumn('receber_newsletter');
            $table->dropColumn('bloqueado');
        });
    }
}
