<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateIntConfiguracoesTable extends Migration
{
    public function up()
    {
        Schema::create('int_configuracoes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->text('keywords');
            $table->string('imagem_de_compartilhamento');
            $table->string('analytics');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('int_configuracoes');
    }
}
