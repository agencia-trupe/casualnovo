<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterContatoTableAddTwitter extends Migration
{
    public function up()
    {
        Schema::table('contato', function(Blueprint $table) {
            $table->string('twitter')->after('pinterest');
        });
    }

    public function down()
    {
        Schema::table('contato', function(Blueprint $table) {
            $table->dropColumn('twitter');
        });
    }
}
