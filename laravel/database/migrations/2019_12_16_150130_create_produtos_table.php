<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosTable extends Migration
{
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('divisao');
            $table->integer('tipos_id')->unsigned()->nullable();
            $table->foreign('tipos_id')->references('id')->on('tipos')->onDelete('CASCADE');
            $table->string('origem');
            $table->string('titulo');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->string('item')->nullable();
            $table->string('item_en')->nullable();
            $table->string('item_es')->nullable();
            $table->string('codigo')->unique();
            $table->string('imagem_capa');
            $table->text('descritivo');
            $table->text('descritivo_en');
            $table->text('descritivo_es');
            $table->string('designer');
            $table->text('dimensoes');
            $table->integer('publicar_site')->nullable();
		 	$table->integer('publicar_catalogo')->nullable();
            $table->integer('mostrar_em_listas')->nullable();
            $table->integer('outlet')->nullable();
            $table->integer('exibir_3d')->nullable();
            $table->integer('linhas_id')->unsigned()->nullable();
            $table->foreign('linhas_id')->references('id')->on('linhas')->onDelete('CASCADE');
            $table->integer('fornecedores_id')->unsigned()->nullable();
            $table->foreign('fornecedores_id')->references('id')->on('fornecedores')->onDelete('CASCADE');
            $table->boolean('brasil')->default(true);
            $table->boolean('internacional')->default(false);
            $table->timestamps();
        });

        Schema::create('produtos_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produtos_id')->unsigned();
            $table->foreign('produtos_id')->references('id')->on('produtos')->onDelete('cascade');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
        });

        Schema::create('produtos_has_produtos_materiais', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('produtos_id')->unsigned()->nullable();
            $table->foreign('produtos_id')->references('id')->on('produtos')->onDelete('CASCADE');
            $table->integer('produtos_materiais_id')->unsigned()->nullable();
            $table->foreign('produtos_materiais_id')->references('id')->on('produtos_materiais')->onDelete('CASCADE');
			$table->timestamps();
		});
    }

    public function down()
    {
        Schema::drop('produtos_has_produtos_materiais');
        Schema::drop('produtos_imagens');
        Schema::drop('produtos');
    }
}
