<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateIntBannersTable extends Migration
{
    public function up()
    {
        Schema::create('int_banners', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('link');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('int_banners');
    }
}
