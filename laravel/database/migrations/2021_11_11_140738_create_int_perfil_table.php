<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateIntPerfilTable extends Migration
{
    public function up()
    {
        Schema::create('int_perfil', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem');
            $table->text('texto_en');
            $table->text('texto_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('int_perfil');
    }
}
