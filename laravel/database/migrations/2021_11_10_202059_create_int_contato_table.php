<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateIntContatoTable extends Migration
{
    public function up()
    {
        Schema::create('int_contato', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->string('telefone');
            $table->string('facebook');
            $table->string('instagram');
            $table->string('pinterest');
            $table->string('twitter');
            $table->string('flickr');
            $table->string('endereco_en');
            $table->string('endereco_es');
            $table->text('google_maps');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('int_contato');
    }
}
