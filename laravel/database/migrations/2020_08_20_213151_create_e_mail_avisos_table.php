<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEMailAvisosTable extends Migration
{
    public function up()
    {
        Schema::create('e_mail_avisos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('e_mail_avisos');
    }
}
