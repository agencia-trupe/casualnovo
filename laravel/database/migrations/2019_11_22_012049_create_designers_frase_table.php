<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignersFraseTable extends Migration
{
    public function up()
    {
        Schema::create('designers_frase', function (Blueprint $table) {
            $table->increments('id');
            $table->string('frase');
            $table->string('frase_en');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('designers_frase');
    }
}
